package menu;

import roulette.ImageManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.CountDownLatch;

public class Menu {

    ImageManager imageManager = new ImageManager();


    JFrame frame;
    JPanel panelMenu;
    JLabel lblFonMenu;
    JLabel lblPoker;
    JLabel lblRoulette;

    JButton btnRoulette;
    JButton btnPoker;


    Color fonColor = new Color(16, 53, 9);
    Color whiteColor = new Color(255, 255, 255);
    Color winColor = new Color(255, 199, 0);

    Font font = new Font("Arial", 1, 70);

    public Menu(JFrame frame) {
        this.frame = frame;


    }

    public int startMenu() {
        CountDownLatch latch = new CountDownLatch(1);

        final int[] value = {0};


        panelMenu = new JPanel();
        frame.add(panelMenu);
        panelMenu.setLayout(null);
        panelMenu.setBounds(0, 0, 1000, 705);

        btnRoulette = new JButton();
        panelMenu.add(btnRoulette);
        btnRoulette.setBounds(20, 250, 80, 80);
        btnRoulette.setIcon(new ImageIcon(imageManager.getImgChips1()));
        btnRoulette.setBackground(fonColor);
        btnRoulette.setOpaque(true);
        btnRoulette.setBorderPainted(false);
        btnRoulette.setFocusPainted(false);
        btnRoulette.setContentAreaFilled(false);
        btnRoulette.setRolloverEnabled(false);

        btnRoulette.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                value[0] = 1;
                frame.remove(panelMenu);
                frame.repaint();
                latch.countDown();
            }
        });

        lblRoulette = new JLabel("Roulette");
        panelMenu.add(lblRoulette);
        lblRoulette.setBounds(124, 260, 400, 60);
        lblRoulette.setForeground(whiteColor);
        lblRoulette.setFont(font);


        btnPoker = new JButton();
        panelMenu.add(btnPoker);
        btnPoker.setBounds(20, 350, 80, 80);
        btnPoker.setIcon(new ImageIcon(imageManager.getImgChips1()));
        btnPoker.setBackground(fonColor);
        btnPoker.setOpaque(true);
        btnPoker.setBorderPainted(false);
        btnPoker.setFocusPainted(false);
        btnPoker.setContentAreaFilled(false);
        btnPoker.setRolloverEnabled(false);

        btnPoker.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                value[0] = 2;
                frame.remove(panelMenu);
                frame.repaint();
                latch.countDown();
            }
        });

        lblPoker = new JLabel("Poker");
        panelMenu.add(lblPoker);
        lblPoker.setBounds(124, 360, 400, 60);
        lblPoker.setForeground(whiteColor);
        lblPoker.setFont(font);


        lblFonMenu = new JLabel();
        panelMenu.add(lblFonMenu);
        lblFonMenu.setBounds(0, 0, 984, 676);
        lblFonMenu.setBackground(fonColor);
        lblFonMenu.setOpaque(true);

        frame.repaint();
        frame.setVisible(true);


        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return value[0];
    }

}


