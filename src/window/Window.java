package window;

import javax.swing.*;
import java.awt.*;

public class Window {


    private JFrame frame;

    private int width = 1000;
    private int height = 715;
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private int x = screenSize.width / 2 - (width / 2);
    private int y = screenSize.height / 2 - (height / 2);


    public Window() {
        frame = new JFrame();
        frame.setBounds(x, y, width, height);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(3);
        frame.setLayout(null);
    }

    public void clearFrame() {
        frame.getContentPane().removeAll();

        frame.repaint();

    }


    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }
}
