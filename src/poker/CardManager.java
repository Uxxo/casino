package poker;

import java.util.ArrayList;
import java.util.List;

public class CardManager {

    List<Card> usingCards;
    List<Card> deckCards;
    List<Card> playersCards;
    List<Card> casinoCards;
    List<Card> flopCards;



    public CardManager(List<Card> deckCards) {
        this.deckCards = deckCards;
        usingCards = new ArrayList<>();
        playersCards = new ArrayList<>();
        casinoCards = new ArrayList<>();
        flopCards = new ArrayList<>();
    }

    public Card generateRandomCard() {
        boolean flag = true;
        Card resultCard = new Card();

        while (flag) {
            int suit = (int) (Math.random() * 4) + 1;
            int faceValue = (int) (Math.random() * 13) + 1;
            for (Card card : deckCards) {
                if (card.getSuit() == suit && card.getFaceValue() == faceValue) {
                    resultCard = card;
                }
            }
            if (!usingCards.contains(resultCard)) {
                flag = false;
                usingCards.add(resultCard);
            }
        }
        return resultCard;
    }

    public List<Card> generatePlayersCards(Card card1, Card card2) {

        playersCards.add(card1);
        playersCards.add(card2);

        return playersCards;
    }

    public List<Card> generateCasinoCards(Card card1, Card card2) {

        casinoCards.add(card1);
        casinoCards.add(card2);

        return casinoCards;
    }

    public List<Card> generateFlopCards(Card card1, Card card2, Card card3, Card card4, Card card5){

        flopCards.add(card1);
        flopCards.add(card2);
        flopCards.add(card3);
        flopCards.add(card4);
        flopCards.add(card5);

        return flopCards;
    }

    public void clearUsingCardList(){
        usingCards.clear();
    }



    public List<Card> getPlayersCards() {
        return playersCards;
    }

    public List<Card> getCasinoCards() {
        return casinoCards;
    }

    public List<Card> getFlopCards() {
        return flopCards;
    }
}


