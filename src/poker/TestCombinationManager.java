package poker;

import money.MoneyManager;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestCombinationManager {

    private static final String ROYAL_FLUSH = "Роял Флеш!!!";

    private static final String STRAIGHT_FLUSH = "Стрит Флеш!!!";

    private static final String FOUR_OF_A_KING = "Каре!!!";

    private static final String FULL_HOUSE = "Фулл Хаус!!!";

    private static final String FLUSH = "Флеш!!!";

    private static final String STRAIGHT = "Стрит!!!";

    private static final String THREE_OF_A_KING = "Трипл!";

    private static final String TWO_PAIR = "Две пары!";

    private static final String PAIR = "Пара!";

    private static final String HIGH_CARD = "Старшая карта";

    public static final String CASINO_WINNER = "У казино ";
    public static final String PLAYER_WINNER = "У Вас ";
    public static final String DRAW = "Ничья ";

    String winner;
    String combination;
    String prize = "";


    @Test
    public void isRoyalFlush_happyPatchTrue() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 1, null);
        Card card2 = new Card(1, 10, null);

        Card card3 = new Card(1, 11, null);
        Card card4 = new Card(1, 12, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //When
        boolean result = combinationManager.isRoyalFlush(testFlopList, testCheckList);

        //Then
        Assert.assertEquals(result, true);

    }

    @Test
    public void isRoyalFlush_happyPatchFalse() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 7, null);
        Card card2 = new Card(1, 10, null);
        Card card3 = new Card(1, 11, null);
        Card card4 = new Card(1, 12, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(1, 2, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //When
        boolean result = combinationManager.isRoyalFlush(testFlopList, testCheckList);

        //Then
        Assert.assertEquals(result, false);

    }

    @Test
    public void isStraightFlush_happyPatch_HaveStraightFlush() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 9, null);
        Card card2 = new Card(1, 10, null);
        Card card3 = new Card(1, 8, null);
        Card card4 = new Card(1, 12, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(1, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //WHEN

        int result = combinationManager.isStraightFlush(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, 10);
    }

    @Test
    public void isStraightFlush_happyPatch_DoNotHaveStraightFlush() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 9, null);
        Card card2 = new Card(2, 10, null);
        Card card3 = new Card(3, 8, null);
        Card card4 = new Card(4, 12, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(1, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //WHEN

        int result = combinationManager.isStraightFlush(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, 0);
    }

    @Test
    public void isFourOfAKing_HappyPatch_HaveFourOfAKing() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 9, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 9, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //WHEN

        int result = combinationManager.isFourOfAKing(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, 9);
    }

    @Test
    public void isFourOfAKing_HappyPatch_DoNotHaveFourOfAKing() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 9, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //WHEN

        int result = combinationManager.isFourOfAKing(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, 0);
    }

    @Test
    public void isFoulHouse_HappyPath_HaveFoulHouse() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 9, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = Arrays.asList(13,13,9,9,9);
        //WHEN

        List<Integer> result = combinationManager.isFoulHouse(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void isFoulHouse_HappyPath_DoNotHaveFoulHouse() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 9, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(1, 11, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(4, 9, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult= new ArrayList<>();
        //WHEN

        List<Integer> result = combinationManager.isFoulHouse(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void isFlush_HappyPatch_HaveFlush() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 9, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(1, 13, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(1, 1, null);
        Card card6 = new Card(1, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(0, 1);
        expectedResult.add(1, 13);
        expectedResult.add(2, 9);
        expectedResult.add(3, 7);
        expectedResult.add(4, 6);


        //WHEN

        List<Integer> result = combinationManager.isFlush(testFlopList, testCheckList);

        //THEN

        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void isFlush_HappyPatch_DoNotHaveFlush() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 9, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(1, 13, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(1, 10, null);
        Card card6 = new Card(1, 7, null);
        Card card7 = new Card(3, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = new ArrayList<>();
        //WHEN

        List<Integer> result = combinationManager.isFlush(testFlopList, testCheckList);

        //THEN

        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void isStraight_HappyPatch_HaveStraight() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 4, null);
        Card card2 = new Card(2, 2, null);
        Card card3 = new Card(3, 6, null);
        Card card4 = new Card(4, 3, null);
        Card card5 = new Card(1, 8, null);
        Card card6 = new Card(2, 5, null);
        Card card7 = new Card(1, 5, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(6);
        expectedResult.add(5);
        expectedResult.add(4);
        expectedResult.add(3);
        expectedResult.add(2);

        //WHEN

        List<Integer> result = combinationManager.isStraight(testFlopList, testCheckList);

        //THEN

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void isStraight_HappyPatch_DoNotHaveStraight() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 10, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 11, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = new ArrayList<>();
        //WHEN

        List<Integer> result = combinationManager.isStraight(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void isThreeOfAKing_HappyPatch_HaveStraight() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 13, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 8, null);
        Card card5 = new Card(2, 13, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //WHEN

        int result = combinationManager.isThreeOfAKing(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, 13);
    }

    @Test
    public void isThreeOfAKing_HappyPatch_DoNotHaveStraight() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 13, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 8, null);
        Card card5 = new Card(2, 5, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //WHEN

        int result = combinationManager.isThreeOfAKing(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, 0);
    }

    @Test
    public void isTwoPare_HappyPatch_HaveTwoPare() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 13, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(3, 9, null);
        Card card6 = new Card(2, 0, null);
        Card card7 = new Card(1, 1, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(13);
        expectedResult.add(9);
        //WHEN

        List<Integer> result = combinationManager.isTwoPair(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void isTwoPare_HappyPatch_HaveTwoPareWithAce() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 13, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(3, 9, null);
        Card card6 = new Card(2, 1, null);
        Card card7 = new Card(1, 1, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(1);
        expectedResult.add(13);
        //WHEN

        List<Integer> result = combinationManager.isTwoPair(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void isTwoPare_HappyPatch_DoNotHaveTwoPare() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 3, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(2, 5, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = new ArrayList<>();
        //WHEN

        List<Integer> result = combinationManager.isTwoPair(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void isPare_HappyPatch_HavePare() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 3, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 9, null);
        Card card5 = new Card(2, 5, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //WHEN

        int result = combinationManager.isPair(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, 9);
    }

    @Test
    public void isPare_HappyPatch_DoNotHavePare() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 3, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 2, null);
        Card card5 = new Card(2, 5, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        //WHEN

        int result = combinationManager.isPair(testFlopList, testCheckList);

        //THEN
        Assert.assertEquals(result, 0);
    }

    @Test
    public void highCard_HappyPatch() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 3, null);
        Card card2 = new Card(2, 9, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 2, null);
        Card card5 = new Card(2, 5, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = Arrays.asList(9,3);

        //WHEN

        List<Integer> result = combinationManager.takeHighCard(testCheckList);

        //THEN
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void highCard_HappyPatchWithAce() throws Exception {
        //GIVEN
        List<Card> testFlopList = new ArrayList<>();
        List<Card> testCheckList = new ArrayList<>();

        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 3, null);
        Card card2 = new Card(2, 1, null);
        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(4, 2, null);
        Card card5 = new Card(2, 5, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);

        testFlopList.add(card3);
        testFlopList.add(card4);
        testFlopList.add(card5);
        testFlopList.add(card6);
        testFlopList.add(card7);

        testCheckList.add(card1);
        testCheckList.add(card2);

        List<Integer> expectedResult = Arrays.asList(1,3);
        //WHEN

        List<Integer> result = combinationManager.takeHighCard(testCheckList);

        //THEN
        Assert.assertEquals(expectedResult,result);
    }


    //-----------------------------------------------------------------------------------------------------------------//

    @Test
    public void checkWinner_HappyPatch_PlayerWin_RoyalFlesh() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 1, null);
        Card card2 = new Card(1, 10, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(1, 11, null);
        Card card4 = new Card(1, 12, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(2, 7, null);
        Card card10 = new Card(2, 7, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = ROYAL_FLUSH;
        int rate = 1;
        String prize = String.valueOf(rate * 100);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_CasinoWin_RoyalFlesh() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 7, null);
        Card card2 = new Card(12, 15, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(1, 11, null);
        Card card4 = new Card(1, 12, null);
        Card card5 = new Card(1, 13, null);
        Card card6 = new Card(2, 7, null);
        Card card7 = new Card(1, 6, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(1, 1, null);
        Card card10 = new Card(1, 10, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = CASINO_WINNER;
        combination = ROYAL_FLUSH;
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_StraightFlesh() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 13, null);
        Card card2 = new Card(1, 12, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(1, 10, null);
        Card card4 = new Card(1, 9, null);
        Card card5 = new Card(1, 11, null);
        Card card6 = new Card(1, 7, null);
        Card card7 = new Card(1, 6, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(1, 5, null);
        Card card10 = new Card(1, 8, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = STRAIGHT_FLUSH;
        int rate = 1;
        String prize = String.valueOf(rate * 50);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }
    @Test
    public void checkWinner_HappyPatch_CasinoWin_StraightFlush() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 13, null);
        Card card2 = new Card(2, 12, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(1, 10, null);
        Card card4 = new Card(1, 9, null);
        Card card5 = new Card(1, 11, null);
        Card card6 = new Card(1, 7, null);
        Card card7 = new Card(1, 6, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(1, 5, null);
        Card card10 = new Card(1, 8, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = CASINO_WINNER;
        combination = STRAIGHT_FLUSH;
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_FourOfAKing() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 13, null);
        Card card2 = new Card(2, 13, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 13, null);
        Card card4 = new Card(1, 9, null);
        Card card5 = new Card(3, 5, null);
        Card card6 = new Card(4, 13, null);
        Card card7 = new Card(4, 5, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(1, 5, null);
        Card card10 = new Card(2, 5, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = FOUR_OF_A_KING;
        int rate = 1;
        String prize = String.valueOf(rate * 40);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }
    @Test
    public void checkWinner_HappyPatch_CasinoWin_FourOfAKing() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 13, null);
        Card card2 = new Card(2, 12, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(1, 10, null);
        Card card4 = new Card(4, 5, null);
        Card card5 = new Card(1, 11, null);
        Card card6 = new Card(3, 5, null);
        Card card7 = new Card(1, 6, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(1, 5, null);
        Card card10 = new Card(2, 5, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = CASINO_WINNER;
        combination = FOUR_OF_A_KING;
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_FullHouse() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 12, null);
        Card card2 = new Card(2, 12, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 12, null);
        Card card4 = new Card(1, 13, null);
        Card card5 = new Card(2, 13, null);
        Card card6 = new Card(4, 13, null);
        Card card7 = new Card(4, 5, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(1, 5, null);
        Card card10 = new Card(2, 5, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = FULL_HOUSE;
        int rate = 1;
        String prize = String.valueOf(rate * 7);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_CasinoWin_FullHouse() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 5, null);
        Card card2 = new Card(2, 5, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 12, null);
        Card card4 = new Card(1, 13, null);
        Card card5 = new Card(2, 13, null);
        Card card6 = new Card(4, 13, null);
        Card card7 = new Card(4, 5, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(1, 12, null);
        Card card10 = new Card(2, 12, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = CASINO_WINNER;
        combination = FULL_HOUSE;
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_Flush() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 2, null);
        Card card2 = new Card(1, 1, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 12, null);
        Card card4 = new Card(1, 3, null);
        Card card5 = new Card(1, 12, null);
        Card card6 = new Card(4, 13, null);
        Card card7 = new Card(1, 5, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(1, 13, null);
        Card card10 = new Card(1, 4, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = FLUSH;
        int rate = 1;
        String prize = String.valueOf(rate * 5);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_CasinoWin_Flush() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(1, 13, null);
        Card card2 = new Card(1, 4, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 12, null);
        Card card4 = new Card(1, 3, null);
        Card card5 = new Card(1, 12, null);
        Card card6 = new Card(4, 13, null);
        Card card7 = new Card(1, 5, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(1, 2, null);
        Card card10 = new Card(1, 1, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = CASINO_WINNER;
        combination = FLUSH;
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_Straight() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 4, null);
        Card card2 = new Card(2, 2, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 6, null);
        Card card4 = new Card(1, 3, null);
        Card card5 = new Card(1, 8, null);
        Card card6 = new Card(4, 5, null);
        Card card7 = new Card(1, 5, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 5, null);
        Card card10 = new Card(1, 2, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = STRAIGHT;
        int rate = 1;
        String prize = String.valueOf(rate * 4);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_CasinoWin_Straight() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 13, null);
        Card card2 = new Card(2, 10, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 9, null);
        Card card4 = new Card(1, 7, null);
        Card card5 = new Card(1, 8, null);
        Card card6 = new Card(4, 1, null);
        Card card7 = new Card(1, 5, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 5, null);
        Card card10 = new Card(1, 6, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = CASINO_WINNER;
        combination = STRAIGHT;
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_ThreeOfAKing() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 10, null);
        Card card2 = new Card(2, 12, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 1, null);
        Card card4 = new Card(1, 10, null);
        Card card5 = new Card(2, 10, null);
        Card card6 = new Card(4, 5, null);
        Card card7 = new Card(1, 11, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 10, null);
        Card card10 = new Card(1, 4, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = THREE_OF_A_KING;
        int rate = 1;
        String prize = String.valueOf(rate * 3);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_CasinoWin_ThreeOfAKing() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 10, null);
        Card card2 = new Card(2, 4, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 1, null);
        Card card4 = new Card(1, 10, null);
        Card card5 = new Card(2, 10, null);
        Card card6 = new Card(4, 5, null);
        Card card7 = new Card(1, 11, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 10, null);
        Card card10 = new Card(1, 12, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = CASINO_WINNER;
        combination = THREE_OF_A_KING;
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_Draw_ThreeOfAKing() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 10, null);
        Card card2 = new Card(2, 12, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 1, null);
        Card card4 = new Card(1, 10, null);
        Card card5 = new Card(2, 10, null);
        Card card6 = new Card(4, 5, null);
        Card card7 = new Card(1, 11, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 10, null);
        Card card10 = new Card(1, 12, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = DRAW;
        combination = "";
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_TwoPare() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 10, null);
        Card card2 = new Card(2, 12, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 1, null);
        Card card4 = new Card(1, 10, null);
        Card card5 = new Card(2, 12, null);
        Card card6 = new Card(4, 5, null);
        Card card7 = new Card(1, 9, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 10, null);
        Card card10 = new Card(1, 9, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = TWO_PAIR;
        int rate = 1;
        String prize = String.valueOf(rate * 2);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_TwoPare_WithHighCard() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 5, null);
        Card card2 = new Card(2, 1, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 12, null);
        Card card4 = new Card(1, 10, null);
        Card card5 = new Card(2, 10, null);
        Card card6 = new Card(4, 12, null);
        Card card7 = new Card(1, 9, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 13, null);
        Card card10 = new Card(1, 9, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = TWO_PAIR;
        int rate = 1;
        String prize = String.valueOf(rate * 2);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_CasinoWin_TwoPair() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 10, null);
        Card card2 = new Card(2, 9, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 1, null);
        Card card4 = new Card(1, 10, null);
        Card card5 = new Card(2, 12, null);
        Card card6 = new Card(4, 5, null);
        Card card7 = new Card(1, 9, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 10, null);
        Card card10 = new Card(1, 12, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = CASINO_WINNER;
        combination = TWO_PAIR;
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_Draw_TwoPair() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 5, null);
        Card card2 = new Card(2, 1, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 12, null);
        Card card4 = new Card(1, 10, null);
        Card card5 = new Card(2, 10, null);
        Card card6 = new Card(4, 12, null);
        Card card7 = new Card(1, 9, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 5, null);
        Card card10 = new Card(1, 1, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = DRAW;
        combination = "";
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_Pare() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 5, null);
        Card card2 = new Card(2, 10, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 2, null);
        Card card4 = new Card(1, 10, null);
        Card card5 = new Card(2, 1, null);
        Card card6 = new Card(4, 4, null);
        Card card7 = new Card(1, 9, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 13, null);
        Card card10 = new Card(1, 8, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = PAIR;
        int rate = 1;
        String prize = String.valueOf(rate);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_PlayerWin_Pare_WithHighCard() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 5, null);
        Card card2 = new Card(2, 10, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 3, null);
        Card card4 = new Card(1, 11, null);
        Card card5 = new Card(2, 11, null);
        Card card6 = new Card(4, 4, null);
        Card card7 = new Card(1, 9, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 6, null);
        Card card10 = new Card(1, 2, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = PLAYER_WINNER;
        combination = PAIR;
        int rate = 1;
        String prize = String.valueOf(rate);

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }

    @Test
    public void checkWinner_HappyPatch_CasinoWin_Pair() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 4, null);
        Card card2 = new Card(2, 9, null);
        testPlayerCardList = createPlayerCardList(card1, card2);

        Card card3 = new Card(3, 1, null);
        Card card4 = new Card(1, 10, null);
        Card card5 = new Card(2, 2, null);
        Card card6 = new Card(4, 5, null);
        Card card7 = new Card(1, 9, null);
        testFlopList = createFlopCardList(card3, card4, card5, card6, card7);

        Card card9 = new Card(3, 10, null);
        Card card10 = new Card(1, 12, null);
        testCasinoCardList = createCasinoCardList(card9, card10);

        winner = CASINO_WINNER;
        combination = PAIR;
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void checkWinner_HappyPatch_Draw_Pair() throws Exception {
        //GIVEN
        List<Card> testFlopList;
        List<Card> testPlayerCardList;
        List<Card> testCasinoCardList;

        MoneyManager moneyManager = new MoneyManager();
        CombinationManager combinationManager = new CombinationManager();

        Card card1 = new Card(4, 5, null);
        Card card2 = new Card(2, 1, null);
        testPlayerCardList = createPlayerCardList(card1,card2);

        Card card3 = new Card(3, 12, null);
        Card card4 = new Card(1, 5, null);
        Card card5 = new Card(2, 10, null);
        Card card6 = new Card(4, 3, null);
        Card card7 = new Card(1, 9, null);
        testFlopList = createFlopCardList(card3,card4,card5,card6,card7);

        Card card9 = new Card(3, 5, null);
        Card card10 = new Card(1, 1, null);
        testCasinoCardList = createCasinoCardList(card9,card10);

        winner = DRAW;
        combination = "";
        int rate = 1;

        List<String> expectedResult = Arrays.asList(winner, combination, prize);

        //WHEN
        List<String> result = combinationManager.checkWinner
                (rate, moneyManager, testPlayerCardList, testCasinoCardList, testFlopList);

        //THEN
        Assert.assertEquals(expectedResult,result );
    }


    private List<Card> createPlayerCardList(Card card1, Card card2) {
        List<Card> playerCardList = new ArrayList<>();
        playerCardList.add(card1);
        playerCardList.add(card2);

        return playerCardList;
    }

    private List<Card> createCasinoCardList(Card card1, Card card2) {
        List<Card> casinoCardList = new ArrayList<>();
        casinoCardList.add(card1);
        casinoCardList.add(card2);

        return casinoCardList;
    }

    private List<Card> createFlopCardList(Card card1, Card card2, Card card3,Card card4,Card card5) {
        List<Card> flopCardList = new ArrayList<>();
        flopCardList.add(card1);
        flopCardList.add(card2);
        flopCardList.add(card3);
        flopCardList.add(card4);
        flopCardList.add(card5);

        return flopCardList;
    }
}
