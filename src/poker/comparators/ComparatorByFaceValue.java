package poker.comparators;

import java.util.Comparator;

public class ComparatorByFaceValue implements Comparator<Integer> {
    @Override
    public int compare(Integer i1, Integer i2) {
        return (i2 - i1);
    }
}
