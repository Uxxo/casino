package poker;

import java.awt.*;
import java.util.Objects;

public class Card {

    private int suit;
    private int faceValue;
    private Image image;

    public Card() {
    }

    public Card(int suit, int faceValue, Image image) {
        this.suit = suit;
        this.faceValue = faceValue;
        this.image = image;
    }

    public int getSuit() {
        return suit;
    }

    public void setSuit(int suit) {
        this.suit = suit;
    }

    public int getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(int faceValue) {
        this.faceValue = faceValue;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return suit == card.suit &&
                faceValue == card.faceValue &&
                Objects.equals(image, card.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(suit, faceValue);
    }
}
