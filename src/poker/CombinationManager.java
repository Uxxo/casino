package poker;

import money.MoneyManager;
import org.w3c.dom.ls.LSOutput;
import poker.comparators.ComparatorByFaceValue;

import java.util.*;

public class CombinationManager {

    private static final String ROYAL_FLUSH = "Роял Флеш!!!";

    private static final String STRAIGHT_FLUSH = "Стрит Флеш!!!";

    private static final String FOUR_OF_A_KING = "Каре!!!";

    private static final String FULL_HOUSE = "Фулл Хаус!!!";

    private static final String FLUSH = "Флеш!!!";

    private static final String STRAIGHT = "Стрит!!!";

    private static final String THREE_OF_A_KING = "Трипл!";

    private static final String TWO_PAIR = "Две пары!";

    private static final String PAIR = "Пара!";

    private static final String HIGH_CARD = "Старшая карта";

    public static final String CASINO_WINNER = "У казино ";
    public static final String PLAYER_WINNER = "У Вас ";
    public static final String DRAW = "Ничья ";

    public CombinationManager() {

    }

    public Combination createCombination(List<Card> cards, List<Card> flopCards) {
        Combination combination = new Combination(isRoyalFlush(flopCards, cards), isStraightFlush(flopCards, cards),
                isFourOfAKing(flopCards, cards), isFoulHouse(flopCards, cards), isFlush(flopCards, cards),
                isStraight(flopCards, cards), isThreeOfAKing(flopCards, cards), isTwoPair(flopCards, cards),
                isPair(flopCards, cards), takeHighCard(cards));

        return combination;
    }

    public List<String> checkWinner(int rate, MoneyManager moneyManager, List<Card> playerCards, List<Card> casinoCards,
                                    List<Card> flopCards) {
        int allCash = moneyManager.getAllCash();
        Combination playerCombination = createCombination(playerCards, flopCards);
        Combination casinoCombination = createCombination(casinoCards, flopCards);

        String winner = "";
        String combination = "";
        String prize = "";


        int moneyWon;
//___________________________________________________Royal Flush______________________________________________________//
        if (playerCombination.isRoyalFlush()) {
            moneyWon = (rate * 100) + rate;
            allCash += moneyWon;
            moneyManager.setAllCash(allCash);

            winner = PLAYER_WINNER;
            combination = ROYAL_FLUSH;
            prize = String.valueOf(moneyWon - rate);

            return createMessage(winner, combination, prize);
        }
        if (casinoCombination.isRoyalFlush()) {

            winner = CASINO_WINNER;
            combination = ROYAL_FLUSH;
            return createMessage(winner, combination, prize);
        }

//______________________________________________Straight Flush________________________________________________________//
        if (playerCombination.getStraightFlush() > 0 &&
                playerCombination.getStraightFlush() > casinoCombination.getStraightFlush()) {
            moneyWon = (rate * 50) + rate;
            allCash += moneyWon;
            moneyManager.setAllCash(allCash);

            winner = PLAYER_WINNER;
            combination = STRAIGHT_FLUSH;
            prize = String.valueOf(moneyWon - rate);

            return createMessage(winner, combination, prize);
        }
        if (casinoCombination.getStraightFlush() > 0 &&
                casinoCombination.getStraightFlush() > playerCombination.getStraightFlush()) {
            winner = CASINO_WINNER;
            combination = STRAIGHT_FLUSH;
            return createMessage(winner, combination, prize);
        }

//____________________________________________________Four Of A King__________________________________________________//
        if (playerCombination.getFourOfAKing() > 0 &&
                playerCombination.getFourOfAKing() > casinoCombination.getFourOfAKing()) {
            moneyWon = (rate * 40) + rate;
            allCash += moneyWon;
            moneyManager.setAllCash(allCash);

            winner = PLAYER_WINNER;

            combination = FOUR_OF_A_KING;
            prize = String.valueOf(moneyWon - rate);

            return createMessage(winner, combination, prize);
        }
        if (casinoCombination.getFourOfAKing() > 0 &&
                casinoCombination.getFourOfAKing() > playerCombination.getFourOfAKing()) {

            winner = CASINO_WINNER;
            combination = FOUR_OF_A_KING;
            return createMessage(winner, combination, prize);
        }

//_______________________________________________________Full House____________________________________________________//
        List<Integer> playerFoulHouse = playerCombination.getFullHouse();
        List<Integer> casinoFoulHouse = casinoCombination.getFullHouse();

        if (playerFoulHouse.size() > 0 && casinoFoulHouse.size() == 0) {
            moneyWon = (rate * 7) + rate;
            allCash += moneyWon;
            moneyManager.setAllCash(allCash);

            winner = PLAYER_WINNER;
            combination = FULL_HOUSE;
            prize = String.valueOf(moneyWon - rate);

            return createMessage(winner, combination, prize);
        }

        if (casinoFoulHouse.size() > 0 && playerFoulHouse.size() == 0) {
            winner = CASINO_WINNER;
            combination = FULL_HOUSE;

            return createMessage(winner, combination, prize);
        }

        if (playerFoulHouse.size() > 0 && casinoFoulHouse.size() > 0) {
            boolean flag = comparisonCardLists(playerFoulHouse, casinoFoulHouse);
            if (flag) {
                moneyWon = (rate * 7) + rate;
                allCash += moneyWon;
                moneyManager.setAllCash(allCash);

                winner = PLAYER_WINNER;
                combination = FULL_HOUSE;
                prize = String.valueOf(moneyWon - rate);
            } else {
                winner = CASINO_WINNER;
                combination = FULL_HOUSE;
            }
            return createMessage(winner, combination, prize);
        }
//__________________________________________________________Flush_____________________________________________________//
        List<Integer> playerFlush = playerCombination.getFlush();
        List<Integer> casinoFlush = casinoCombination.getFlush();

        if (playerFlush.size() > casinoFlush.size()) {
            moneyWon = (rate * 5) + rate;
            allCash += moneyWon;
            moneyManager.setAllCash(allCash);

            winner = PLAYER_WINNER;
            combination = FLUSH;
            prize = String.valueOf(moneyWon - rate);

            return createMessage(winner, combination, prize);
        }
        if (casinoFlush.size() > playerFlush.size()) {
            winner = CASINO_WINNER;
            combination = FLUSH;
            return createMessage(winner, combination, prize);
        }
        if (playerFlush.size() > 0 && casinoFlush.size() > 0) {

            boolean win = comparisonCardLists(playerFlush, casinoFlush);
            if (win) {
                moneyWon = (rate * 5) + rate;
                allCash += moneyWon;
                moneyManager.setAllCash(allCash);

                winner = PLAYER_WINNER;
                combination = FLUSH;
                prize = String.valueOf(moneyWon - rate);

                return createMessage(winner, combination, prize);
            } else {
                winner = CASINO_WINNER;
                combination = FLUSH;
                return createMessage(winner, combination, prize);
            }
        }

//__________________________________________________________Straight__________________________________________________//
        List<Integer> playerStraight = playerCombination.getStraight();
        List<Integer> casinoStraight = casinoCombination.getStraight();
        if (playerStraight.size() > 0 && casinoStraight.size() == 0) {
            moneyWon = (rate * 4) + rate;
            allCash += moneyWon;
            moneyManager.setAllCash(allCash);

            winner = PLAYER_WINNER;
            combination = STRAIGHT;
            prize = String.valueOf(moneyWon - rate);

            return createMessage(winner, combination, prize);
        }
        if (casinoStraight.size() > 0 && playerStraight.size() == 0) {
            winner = CASINO_WINNER;
            combination = STRAIGHT;
            return createMessage(winner, combination, prize);
        }
        if (playerStraight.size() > 0 && casinoStraight.size() > 0) {

            boolean win = comparisonCardLists(playerStraight, casinoStraight);
            if (win) {
                moneyWon = (rate * 4) + rate;
                allCash += moneyWon;
                moneyManager.setAllCash(allCash);

                winner = PLAYER_WINNER;
                combination = STRAIGHT;
                prize = String.valueOf(moneyWon - rate);

            } else {
                winner = CASINO_WINNER;
                combination = STRAIGHT;
            }
            return createMessage(winner, combination, prize);
        }

//__________________________________________________________Three Of A King___________________________________________//
        int playerValueThreeOfAKing = playerCombination.getThreeOfAKing();
        int casinoValueThreeOfAKing = casinoCombination.getThreeOfAKing();
        if (playerValueThreeOfAKing > casinoValueThreeOfAKing) {
            moneyWon = (rate * 3) + rate;
            allCash += moneyWon;
            moneyManager.setAllCash(allCash);

            winner = PLAYER_WINNER;
            combination = THREE_OF_A_KING;
            prize = String.valueOf(moneyWon - rate);

            return createMessage(winner, combination, prize);
        }
        if (casinoValueThreeOfAKing > playerValueThreeOfAKing) {
            winner = CASINO_WINNER;
            combination = THREE_OF_A_KING;
            return createMessage(winner, combination, prize);
        }
        if (playerValueThreeOfAKing == casinoValueThreeOfAKing && playerValueThreeOfAKing > 0) {
            List<Integer> playerCardsList = playerCombination.getHighCard();
            List<Integer> casinoCardsList = casinoCombination.getHighCard();

            if (playerCardsList.get(0).equals(casinoCardsList.get(0))
                    && playerCardsList.get(1).equals(casinoCardsList.get(1))) {
                winner = DRAW;
                combination = "";
                allCash += rate;
                moneyManager.setAllCash(allCash);
                return createMessage(winner, combination, prize);
            } else {
                if (comparisonCardLists(playerCardsList, casinoCardsList)) {
                    moneyWon = (rate * 3) + rate;
                    allCash += moneyWon;
                    moneyManager.setAllCash(allCash);

                    winner = PLAYER_WINNER;
                    combination = THREE_OF_A_KING;
                    prize = String.valueOf(moneyWon - rate);

                    return createMessage(winner, combination, prize);
                }
                if (!comparisonCardLists(playerCardsList, casinoCardsList)) {
                    winner = CASINO_WINNER;
                    combination = THREE_OF_A_KING;
                    return createMessage(winner, combination, prize);
                }
            }
        }

//___________________________________________________________Two Pairs________________________________________________//
        List<Integer> playerTwoParesList = playerCombination.getTwoPair();
        List<Integer> casinoTwoParesList = casinoCombination.getTwoPair();
        if (playerTwoParesList.size() > 0 && casinoTwoParesList.size() == 0) {
            moneyWon = (rate * 2) + rate;
            allCash += moneyWon;
            moneyManager.setAllCash(allCash);

            winner = PLAYER_WINNER;
            combination = TWO_PAIR;
            prize = String.valueOf(moneyWon - rate);

            return createMessage(winner, combination, prize);
        }
        if (casinoTwoParesList.size() > 0 && playerTwoParesList.size() == 0) {
            winner = CASINO_WINNER;
            combination = TWO_PAIR;
            return createMessage(winner, combination, prize);
        }
        if (playerTwoParesList.size() > 0 && casinoTwoParesList.size() > 0) {
            if (playerTwoParesList.get(0) > casinoTwoParesList.get(0)) {
                moneyWon = (rate * 2) + rate;
                allCash += moneyWon;
                moneyManager.setAllCash(allCash);

                winner = PLAYER_WINNER;
                combination = TWO_PAIR;
                prize = String.valueOf(moneyWon - rate);

                return createMessage(winner, combination, prize);
            }
            if (playerTwoParesList.get(1) > casinoTwoParesList.get(1)) {
                moneyWon = (rate * 2) + rate;
                allCash += moneyWon;
                moneyManager.setAllCash(allCash);

                winner = PLAYER_WINNER;
                combination = TWO_PAIR;
                prize = String.valueOf(moneyWon - rate);

                return createMessage(winner, combination, prize);
            }

            if (casinoTwoParesList.get(0) > playerTwoParesList.get(0)) {
                winner = CASINO_WINNER;
                combination = TWO_PAIR;
                return createMessage(winner, combination, prize);
            }

            if (casinoTwoParesList.get(1) > playerTwoParesList.get(1)) {
                winner = CASINO_WINNER;
                combination = TWO_PAIR;
                return createMessage(winner, combination, prize);
            }

            if (playerTwoParesList.get(0) == casinoTwoParesList.get(0) &&
                    playerTwoParesList.get(1) == casinoTwoParesList.get(1)) {
//                if (playerCombination.getHighCard() == 1 && casinoCombination.getHighCard() != 1) {
                List<Integer> playerCardsList = playerCombination.getHighCard();
                List<Integer> casinoCardsList = casinoCombination.getHighCard();

                if (playerCardsList.get(0).equals(casinoCardsList.get(0))
                        && playerCardsList.get(1).equals(casinoCardsList.get(1))) {
                    winner = DRAW;
                    combination = "";
                    allCash += rate;
                    moneyManager.setAllCash(allCash);
                    return createMessage(winner, combination, prize);
                } else {
                    if (comparisonCardLists(playerCardsList, casinoCardsList)) {
                        moneyWon = (rate * 2) + rate;
                        allCash += moneyWon;
                        moneyManager.setAllCash(allCash);

                        winner = PLAYER_WINNER;
                        combination = TWO_PAIR;
                        prize = String.valueOf(moneyWon - rate);

                        return createMessage(winner, combination, prize);
                    }
                    if (!comparisonCardLists(playerCardsList, casinoCardsList)) {
                        winner = CASINO_WINNER;
                        combination = TWO_PAIR;
                        return createMessage(winner, combination, prize);
                    }
                }
            }
        }
//______________________________________________________________Pair__________________________________________________//
        if (playerCombination.getPair() > casinoCombination.getPair()) {
            moneyWon = (rate * 2);
            allCash += moneyWon;
            moneyManager.setAllCash(allCash);

            winner = PLAYER_WINNER;
            combination = PAIR;
            prize = String.valueOf(moneyWon - rate);

            return createMessage(winner, combination, prize);
        }
        if (casinoCombination.getPair() > playerCombination.getPair()) {
            winner = CASINO_WINNER;
            combination = PAIR;
            return createMessage(winner, combination, prize);
        }
        if (casinoCombination.getPair() == casinoCombination.getPair() && casinoCombination.getPair() > 0) {

            List<Integer> playerCardsList = playerCombination.getHighCard();
            List<Integer> casinoCardsList = casinoCombination.getHighCard();

            if (playerCardsList.get(0).equals(casinoCardsList.get(0))
                    && playerCardsList.get(1).equals(casinoCardsList.get(1))) {
                winner = DRAW;
                combination = "";
                allCash += rate;
                moneyManager.setAllCash(allCash);
                return createMessage(winner, combination, prize);
            } else {
                if (comparisonCardLists(playerCardsList, casinoCardsList)) {
                    moneyWon = (rate * 2);
                    allCash += moneyWon;
                    moneyManager.setAllCash(allCash);

                    winner = PLAYER_WINNER;
                    combination = PAIR;
                    prize = String.valueOf(moneyWon - rate);

                    return createMessage(winner, combination, prize);
                }
                if (!comparisonCardLists(playerCardsList, casinoCardsList)) {
                    winner = CASINO_WINNER;
                    combination = PAIR;
                    return createMessage(winner, combination, prize);
                }
            }

        }

        winner = "У диллера нет игры.";
        allCash += (rate / 2);
        moneyManager.setAllCash(allCash);
        return createMessage(winner, combination, prize);
    }


    public boolean isRoyalFlush(List<Card> flopCard, List<Card> checkCard) {

        boolean result;

        List<Card> workingList = new ArrayList<>();
        workingList.addAll(flopCard);
        workingList.addAll(checkCard);
        List<Card> suit1 = new ArrayList<>();
        List<Card> suit2 = new ArrayList<>();
        List<Card> suit3 = new ArrayList<>();
        List<Card> suit4 = new ArrayList<>();

        for (Card card : workingList) {
            int suit = card.getSuit();
            if (suit == 1) {
                suit1.add(card);
            }
            if (suit == 2) {
                suit2.add(card);
            }
            if (suit == 3) {
                suit3.add(card);
            }
            if (suit == 4) {
                suit4.add(card);
            }
        }

        if (!containRoyalFlush(suit1) && !containRoyalFlush(suit2) && !containRoyalFlush(suit3) &&
                !containRoyalFlush(suit4)) {
            return false;
        } else {
            return true;
        }
    }

    public boolean containRoyalFlush(List<Card> cardsOnSuit) {
        boolean isRoyalFlush = false;

        if (cardsOnSuit.size() < 5) {
        } else {
            List<Integer> faceValueList = new ArrayList<>();
            for (Card card : cardsOnSuit) {
                int faceValue = card.getFaceValue();
                faceValueList.add(faceValue);
            }
            if (faceValueList.contains(10) && faceValueList.contains(11) && faceValueList.contains(12) &&
                    faceValueList.contains(13) && faceValueList.contains(1)) {
                isRoyalFlush = true;
                return isRoyalFlush;
            }
        }
        return isRoyalFlush;
    }

    public int isStraightFlush(List<Card> flopCard, List<Card> checkCard) {

        int valueOfMaxFaceValue = 0;

        List<Card> workingList = new ArrayList<>();
        workingList.addAll(flopCard);
        workingList.addAll(checkCard);
        List<Card> suit1 = new ArrayList<>();
        List<Card> suit2 = new ArrayList<>();
        List<Card> suit3 = new ArrayList<>();
        List<Card> suit4 = new ArrayList<>();

        for (Card card : workingList) {
            int suit = card.getSuit();
            if (suit == 1) {
                suit1.add(card);
            }
            if (suit == 2) {
                suit2.add(card);
            }
            if (suit == 3) {
                suit3.add(card);
            }
            if (suit == 4) {
                suit4.add(card);
            }
        }

        if (suit1.size() > 4) {
            valueOfMaxFaceValue = containStraightFlush(suit1);
        }
        if (suit2.size() > 4) {
            valueOfMaxFaceValue = containStraightFlush(suit2);
        }
        if (suit3.size() > 4) {
            valueOfMaxFaceValue = containStraightFlush(suit3);
        }
        if (suit4.size() > 4) {
            valueOfMaxFaceValue = containStraightFlush(suit4);
        }

        return valueOfMaxFaceValue;
    }

    public int containStraightFlush(List<Card> cardsOnSuit) {

        int valueOfMaxFaceValue = 0;

        List<Integer> faceValueList = new ArrayList<>();
        for (Card card : cardsOnSuit) {
            int faceValue = card.getFaceValue();
            faceValueList.add(faceValue);
        }

        ComparatorByFaceValue comparator = new ComparatorByFaceValue();
        Collections.sort(faceValueList, comparator);

        int i = 0;
        while (i < faceValueList.size() - 4) {
            int value = faceValueList.get(i);
            if (faceValueList.contains(value - 1) && faceValueList.contains(value - 2) && faceValueList.contains(value - 3)
                    && faceValueList.contains(value - 4)) {
                valueOfMaxFaceValue = value;
                return valueOfMaxFaceValue;
            } else {
                i++;
            }
        }

        return valueOfMaxFaceValue;
    }

    public int isFourOfAKing(List<Card> flopCard, List<Card> checkCard) {

        int valueOfMaxFaceValue = 0;

        List<Card> workingList = new ArrayList<>();
        workingList.addAll(flopCard);
        workingList.addAll(checkCard);


        for (Card card : workingList) {
            int value = card.getFaceValue();
            int counter = 0;
            for (int i = 0; i < workingList.size(); i++) {
                if (workingList.get(i).getFaceValue() == value) {
                    counter++;
                    if (counter == 4) {
                        valueOfMaxFaceValue = value;
                        return valueOfMaxFaceValue;
                    }
                }

            }
        }
        return valueOfMaxFaceValue;
    }

    public List<Integer> isFoulHouse(List<Card> flopCard, List<Card> checkCard) {
        List<Integer> foulHouse = new ArrayList<>();

        List<Card> workListCards = new ArrayList<>();
        workListCards.addAll(flopCard);
        workListCards.addAll(checkCard);

        List<Integer> faceValues = new ArrayList<>();
        for (Card card : workListCards) {
            faceValues.add(card.getFaceValue());
        }

        int counterPairs = 0;
        int counterTriples = 0;

        ComparatorByFaceValue comparator = new ComparatorByFaceValue();
        Collections.sort(faceValues, comparator);

        int i = 0;
        while (i < faceValues.size()) {
            int value = faceValues.get(i);
            int count = Collections.frequency(faceValues, value);
            if (count < 2) {
                faceValues.remove(i);
            } else {
                i++;
            }
        }

        if (faceValues.contains(1)) {
            int count = Collections.frequency(faceValues, 1);
            while (faceValues.contains(1)) {
                int index = faceValues.indexOf(1);
                faceValues.remove(index);
            }
            i = 0;
            while (i < count) {
                faceValues.add(0, 1);
                i++;
            }
        }

        while (faceValues.size() > 5) {
            faceValues.remove(5);
        }

        i = 0;
        while (i < faceValues.size()) {
            int value = faceValues.get(i);
            int count = Collections.frequency(faceValues, value);

            if (count == 2) {
                counterPairs++;
                i += 2;
            }
            if (count == 3) {
                counterTriples++;

                i += 3;
            }
            i++;
        }

        if (counterTriples > 0 && counterPairs > 0) {
            foulHouse = faceValues;
        }

        return foulHouse;
    }

    public List<Integer> isFlush(List<Card> flopCard, List<Card> checkCard) {

        List<Integer> flushList = new ArrayList<>();


        List<Card> workingList = new ArrayList<>();
        workingList.addAll(flopCard);
        workingList.addAll(checkCard);
        List<Card> suit1 = new ArrayList<>();
        List<Card> suit2 = new ArrayList<>();
        List<Card> suit3 = new ArrayList<>();
        List<Card> suit4 = new ArrayList<>();

        for (Card card : workingList) {
            int suit = card.getSuit();
            if (suit == 1) {
                suit1.add(card);
            }
            if (suit == 2) {
                suit2.add(card);
            }
            if (suit == 3) {
                suit3.add(card);
            }
            if (suit == 4) {
                suit4.add(card);
            }
        }

        if (suit1.size() > 4) {
            flushList = containsFlush(suit1);
        }
        if (suit2.size() > 4) {
            flushList = containsFlush(suit2);
        }
        if (suit3.size() > 4) {
            flushList = containsFlush(suit3);
        }
        if (suit4.size() > 4) {
            flushList = containsFlush(suit4);
        }

        return flushList;
    }

    public List<Integer> containsFlush(List<Card> cardsOnSuit) {

        List<Integer> flushList = new ArrayList<>();


        List<Integer> faceValueList = new ArrayList<>();

        for (Card card : cardsOnSuit) {
            int faceValue = card.getFaceValue();
            faceValueList.add(faceValue);
        }

        ComparatorByFaceValue comparator = new ComparatorByFaceValue();
        Collections.sort(faceValueList, comparator);

        if (faceValueList.contains(1)) {
            flushList.add(0, 1);
            int index = faceValueList.indexOf(1);
            faceValueList.remove(index);
            flushList.addAll(faceValueList);
        } else {
            flushList.addAll(faceValueList);
        }

        return flushList;
    }

    public List<Integer> isStraight(List<Card> flopCard, List<Card> checkCard) {

        List<Integer> straightList = new ArrayList<>();

        List<Card> workingList = new ArrayList<>();
        workingList.addAll(flopCard);
        workingList.addAll(checkCard);

        List<Integer> faceValueList = new ArrayList<>();

        for (Card card : workingList) {
            int value = card.getFaceValue();
            if (!faceValueList.contains(value)) {
                faceValueList.add(value);
            }
        }

        if (faceValueList.contains(10) && faceValueList.contains(11) && faceValueList.contains(12)
                && faceValueList.contains(13) && faceValueList.contains(1)) {
            straightList.add(1);
            straightList.add(13);
            straightList.add(12);
            straightList.add(11);
            straightList.add(10);
            return straightList;
        }

        ComparatorByFaceValue comparator = new ComparatorByFaceValue();
        Collections.sort(faceValueList, comparator);


        int i = 0;
        while (faceValueList.size() >= 5) {
            int maxValue = faceValueList.get(i);
            if (faceValueList.contains(maxValue - 1) && faceValueList.contains(maxValue - 2) &&
                    faceValueList.contains(maxValue - 3) && faceValueList.contains(maxValue - 4)) {

                for (int a = 0; a < 5; a++) {
                    straightList.add(faceValueList.get(a));

                }
                return straightList;
            } else {
                int index = faceValueList.indexOf(maxValue);
                faceValueList.remove(index);
            }
        }

        return straightList;
    }

    public int isThreeOfAKing(List<Card> flopCard, List<Card> checkCard) {

        int valueOfMaxFaceValue = 0;

        List<Card> workingList = new ArrayList<>();
        workingList.addAll(flopCard);
        workingList.addAll(checkCard);


        for (Card card : workingList) {
            int value = card.getFaceValue();
            int counter = 0;
            for (int i = 0; i < workingList.size(); i++) {
                if (workingList.get(i).getFaceValue() == value) {
                    counter++;
                    if (counter == 3) {
                        valueOfMaxFaceValue = value;
                        return valueOfMaxFaceValue;
                    }
                }

            }
        }


        return valueOfMaxFaceValue;
    }

    public List<Integer> isTwoPair(List<Card> flopCard, List<Card> checkCard) {

        List<Integer> pairList = new ArrayList<>();

        int counterOfPares = 0;

        List<Card> workingList = new ArrayList<>();
        workingList.addAll(flopCard);
        workingList.addAll(checkCard);

        List<Integer> faceValueList = new ArrayList<>();
        for (Card card : workingList) {
            faceValueList.add(card.getFaceValue());
        }

        ComparatorByFaceValue comparator = new ComparatorByFaceValue();
        Collections.sort(faceValueList, comparator);

        int i = 0;
        while (i < faceValueList.size()) {
            int value = faceValueList.get(i);
            int counter = Collections.frequency(faceValueList, value);
            if (counter > 1) {
                counterOfPares++;
                pairList.add(value);
                i += counter;
            } else {
                i++;
            }
        }

        if (pairList.size() >= 2) {
            if (pairList.contains(1)) {
                int index = pairList.indexOf(1);
                pairList.remove(index);
                pairList.add(0, 1);

            }
            while (pairList.size() > 2) {
                pairList.remove(pairList.size() - 1);
            }
            return pairList;

        } else pairList.clear();

        return pairList;
    }

    public int isPair(List<Card> flopCard, List<Card> checkCard) {

        int valuesOfMaxFaceValue = 0;

        List<Card> workingList = new ArrayList<>();
        workingList.addAll(flopCard);
        workingList.addAll(checkCard);

        List<Card> repeatingCard = new ArrayList<>();

        for (Card card : workingList) {

            if (!repeatingCard.contains(card)) {
                int value = card.getFaceValue();
                int counter = 0;
                for (int i = 0; i < workingList.size(); i++) {
                    if (workingList.get(i).getFaceValue() == value) {
                        repeatingCard.add(workingList.get(i));
                        counter++;
                        if (counter == 2) {
                            valuesOfMaxFaceValue = value;
                            return valuesOfMaxFaceValue;

                        }
                    }
                }
            }
        }

        return valuesOfMaxFaceValue;
    }


    public List<Integer> takeHighCard(List<Card> checkCard) {
        List<Integer> valuesCardsList = new ArrayList<>();

        for (Card newCard : checkCard) {
            valuesCardsList.add(newCard.getFaceValue());
        }
        valuesCardsList.sort(new ComparatorByFaceValue());
        if (valuesCardsList.get(1) == 1) {
            valuesCardsList.remove(1);
            valuesCardsList.add(0, 1);
        }

        return valuesCardsList;
    }

    public boolean comparisonCardLists(List<Integer> playerList, List<Integer> casinoList) {
        boolean flag = false;
        int i = 0;
        if (playerList.get(i) == 1 && casinoList.get(i) != 1) {
            return true;
        }
        if (casinoList.get(i) == 1 && playerList.get(i) != 1) {
            return false;
        }
        while (i < playerList.size()) {
            if (playerList.get(i) > casinoList.get(i)) {
                return true;
            }
            if (playerList.get(i) < casinoList.get(i)) {
                return false;
            }
            i++;
        }
        return flag;
    }

    public List<String> createMessage(String winner, String combination, String prize) {
        List<String> message = new ArrayList<>();
        message.add(0, winner);
        message.add(1, combination);
        message.add(prize);

        return message;
    }

    public boolean checkDialerGame(List<Card> dealerCardsList, List<Card> flopCardsList) {
        Combination casinoCombination = createCombination(dealerCardsList, flopCardsList);

        if (casinoCombination.isRoyalFlush()) {
            return true;
        }
        if (casinoCombination.getStraightFlush() > 0) {
            return true;
        }
        if (casinoCombination.getFourOfAKing() > 0) {
            return true;
        }
        if (casinoCombination.getFullHouse().size() > 0) {
            return true;
        }
        if (casinoCombination.getFlush().size() > 0) {
            return true;
        }
        if (casinoCombination.getStraight().size() > 0) {
            return true;
        }
        if (casinoCombination.getThreeOfAKing() > 0) {
            return true;
        }
        if (casinoCombination.getTwoPair().size() > 0) {
            return true;
        }
        if (casinoCombination.getPair() > 0) {
            return true;
        }
        return false;
    }
}
