package poker;

import money.MoneyManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import roulette.ImageManager;

public class PlayPoker {

    private List<Card> playersCardsList;
    private List<Card> dealerCardsList;
    private List<Card> flopCardsList;


    MoneyManager moneyManager;
    CardManager cardManager;
    ImageManager imageManager = new ImageManager();
    CombinationManager combinationManager = new CombinationManager();

    final int[] valueMenu = {0};
    private int allCash;
    private int rateCost;
    private int bank;

    private int stage;

    private String message;
    private String btnBankMessage;

    private boolean flag;

    CountDownLatch latch;

    Image imgBackSide = imageManager.getImgBackSideCard();

    Color fonColor = new Color(16, 53, 9);
    Color whiteColor = new Color(255, 255, 255);
    Color winColor = new Color(255, 199, 0);

    Font font = new Font("Arial", 1, 70);

    JFrame frame;

    JPanel panelPlayerCards;
    JPanel panelCasinoFields;
    JPanel panelFlopCards;
    JPanel panelPlayerField;
    JPanel panelPlayerLabels;

    JLabel lblCard;
    JLabel lblAllCash;
    JLabel lblAllRateCost;
    JLabel lblMessage;

    JLabel lblCostChips1;
    JLabel lblCostChips2;
    JLabel lblCostChips3;
    JLabel lblCostChips4;
    JLabel lblCostChips5;

    JButton btnBank;
    JButton btnPass;
    JButton btnExit;
    JButton btnPlay;

    JRadioButton rbtChip1;
    JRadioButton rbtChip2;
    JRadioButton rbtChip3;
    JRadioButton rbtChip4;
    JRadioButton rbtChip5;

    Image imgChips;
    Image imgLtlChips;

    private int widthCard = 160;
    private int heightCard = 224;

    int iteration;
    int round;

    public PlayPoker(JFrame frame, MoneyManager moneyManager) {
        this.frame = frame;
        this.moneyManager = moneyManager;

        imgChips = imageManager.getImgChips5();
        imgLtlChips = imageManager.getImgLtlChips5();

        cardManager = new CardManager(new CardFactory().createDeckCard());

        frame.setTitle("Poker");
        frame.revalidate();

        stage = 1;
        rateCost = 5;

        iteration = 0;
        round = 1;
    }

    public void frameRestart() {

        frame.remove(panelPlayerCards);
        frame.remove(panelCasinoFields);
        frame.remove(panelFlopCards);
        frame.remove(panelPlayerLabels);
        frame.remove(panelPlayerField);


        frame.revalidate();
//        frame.repaint();

    }

    public int createGamePlay() {
         latch = new CountDownLatch(1);
        valueMenu[0] = 0;
        allCash = moneyManager.getAllCash();

        if (stage == 1) {
            cardManager.clearUsingCardList();
        }

        createPanelPlayerField();

        try {
            Thread.sleep(10);

            createPanelPlayerCards();
            Thread.sleep(10);

            createPanelCasinoFields();
            Thread.sleep(10);

            createPanelFlopCards();
            Thread.sleep(10);

            createPanelPlayerLabels();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        frame.revalidate();


        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (valueMenu[0] == 0){
            frameRestart();
        }
        return valueMenu[0];
    }

    public void createPanelPlayerCards() {

        if (stage == 1) {
            playersCardsList = cardManager.generatePlayersCards(cardManager.generateRandomCard(),
                    cardManager.generateRandomCard());
        }

        panelPlayerCards = new JPanel();
        frame.add(panelPlayerCards);
        panelPlayerCards.setLayout(null);
        panelPlayerCards.setBounds(145, 450, 555, 226);
        panelPlayerCards.setBackground(fonColor);

        if (stage > 1) {
            int x = 165;
            int y = 1;
            int distance = 10;
            for (Card card : playersCardsList) {

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                lblCard = new JLabel();
                panelPlayerCards.add(lblCard);
                lblCard.setBounds(x, y, widthCard, heightCard);
                lblCard.setBorder(BorderFactory.createLineBorder(fonColor));
                lblCard.setIcon(new ImageIcon(card.getImage()));
                x += distance + widthCard;
            }
        }
        frame.setVisible(true);
    }

    public void createPanelCasinoFields() {
        if (stage == 1) {
            dealerCardsList = cardManager.generateCasinoCards(cardManager.generateRandomCard(),
                    cardManager.generateRandomCard());
        }

        panelCasinoFields = new JPanel();
        frame.add(panelCasinoFields);
        panelCasinoFields.setLayout(null);
        panelCasinoFields.setBounds(0, 0, 1000, 226);
        panelCasinoFields.setBackground(fonColor);

        if (stage > 1) {
            int x = 310;
            int y = 0;
            int distance = widthCard + 10;
            for (Card card : dealerCardsList) {
                lblCard = new JLabel();
                panelCasinoFields.add(lblCard);
                lblCard.setBounds(x, y, widthCard, heightCard);
                lblCard.setBorder(BorderFactory.createLineBorder(fonColor));
                if (stage == 3) {
                    lblCard.setIcon(new ImageIcon(card.getImage()));
                } else {
                    lblCard.setIcon(new ImageIcon(imgBackSide));
                }
                x += distance;
            }
        }

        if (stage == 1) {
            message = "Делайте вашу ставку";
        }

        if (stage == 2) {
            message = "<html> Чтобы продолжать,<br> удваивайте ставку</html>";
        }

        lblMessage = new JLabel(message);
        panelCasinoFields.add(lblMessage);
        lblMessage.setBounds(677, 0, 333, 220);
        lblMessage.setForeground(whiteColor);
        lblMessage.setHorizontalTextPosition(SwingConstants.CENTER);

        lblMessage.setFont(new Font("Arial", Font.BOLD, 25));


        if (stage == 3) {
            if (!combinationManager.checkDialerGame(dealerCardsList, flopCardsList)) {
                System.out.println("ЗАШЁЛ");
                message = "<html> У дилера нет игры." + "<br>" + bank / 2 + "$ </html>";
                allCash += bank;
                moneyManager.setAllCash(allCash);


            } else {
                List<String> resultMessageList = combinationManager.checkWinner(bank, moneyManager, playersCardsList,
                        dealerCardsList, flopCardsList);

                if (!resultMessageList.get(2).isEmpty()) {
                    message = "<html>" + resultMessageList.get(0)
                            + "<br>" + resultMessageList.get(1)
                            + "<br>     +" + resultMessageList.get(2) + "$ </html>";

                } else {
                    message = "<html>" + resultMessageList.get(0)
                            + "<br>" + resultMessageList.get(1) + "</html>";
                }
            }


            lblMessage = new JLabel(message);
            panelCasinoFields.add(lblMessage);
            lblMessage.setBounds(677, 0, 333, 220);
            lblMessage.setForeground(whiteColor);
            lblMessage.setHorizontalTextPosition(SwingConstants.CENTER);

            lblMessage.setFont(new Font("Arial", Font.BOLD, 25));
        }
        frame.setVisible(true);
    }

    public void createPanelFlopCards() {
        if (stage == 1) {
            flopCardsList = cardManager.generateFlopCards(cardManager.generateRandomCard(), cardManager.generateRandomCard()
                    , cardManager.generateRandomCard(), cardManager.generateRandomCard(), cardManager.generateRandomCard());
        }

        panelFlopCards = new JPanel();
        frame.add(panelFlopCards);
        panelFlopCards.setLayout(null);
        panelFlopCards.setBounds(140, 226, 865, 226);
        panelFlopCards.setBackground(fonColor);

        int x = 2;
        int y = 0;
        int distance = 10;

        if (stage > 1) {
            int counter = 0;
            for (Card card : flopCardsList) {

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                lblCard = new JLabel();
                panelFlopCards.add(lblCard);
                lblCard.setBounds(x, y, widthCard, heightCard);

                if (stage == 2 && counter < 3) {
                    lblCard.setIcon(new ImageIcon(card.getImage()));
                } else {
                    lblCard.setIcon(new ImageIcon(imgBackSide));
                }
                if (stage == 3) {
                    lblCard.setIcon(new ImageIcon(card.getImage()));
                }
                x += distance + widthCard;
                counter++;
            }
        }
        frame.setVisible(true);
    }


    public void createPanelPlayerField() {

        panelPlayerField = new JPanel();
        frame.add(panelPlayerField);
        panelPlayerField.setBounds(0, 126, 145, 551);
        panelPlayerField.setBackground(fonColor);
        panelPlayerField.setLayout(null);

        if (stage == 1) {

            btnBankMessage = "BANK";
        }
        if (stage == 2) {
            btnBankMessage = "x2";
        }
        if (stage == 3) {
            btnBankMessage = "OK";
        }
        btnBank = new JButton(btnBankMessage);

        panelPlayerField.add(btnBank);
        btnBank.setBounds(5, 105, 135, 100);
        btnBank.setLayout(new FlowLayout());
        btnBank.setFont(new Font("Arial", Font.BOLD, 40));
        btnBank.setForeground(whiteColor);
        btnBank.setBackground(fonColor);
        btnBank.setBorder(BorderFactory.createLineBorder(whiteColor));

        btnBank.addActionListener(actionEvent -> {
            allCash = moneyManager.getAllCash();
            if (stage == 1) {
                if ((rateCost + bank) <= allCash - rateCost) {
                    bank += rateCost;


                    allCash -= rateCost;
                    moneyManager.setAllCash(allCash);

                    JLabel lblIconChips = new JLabel(new ImageIcon(imgLtlChips));
                    lblIconChips.setOpaque(true);
                    lblIconChips.setBackground(fonColor);

                    btnBank.add(lblIconChips);
                    btnBank.setOpaque(true);
                    btnBank.setBackground(null);
                    btnBank.setContentAreaFilled(true);
                    btnPlay.setVisible(true);
                    btnExit.setVisible(false);
                    btnPass.setVisible(true);

                    frame.setVisible(true);

                    updateFieldPanels(allCash, bank);

                } else {
                    lblMessage.setText("<html> Вы не сможете,<br> удвоить ставку! </html>");

                }
            }
            if (stage == 2) {

                allCash -= bank;
                bank += bank;

                moneyManager.setAllCash(allCash);
                message = "";
                lblMessage.setText(message);
                btnBank.setVisible(false);
                btnPlay.setVisible(true);

                updateFieldPanels(allCash, bank);

            }
            if (stage == 3) {
                clearStage();
                valueMenu [0] = 1;
                latch.countDown();
            }
        });

        btnPass = new JButton("Pass");
        panelPlayerField.add(btnPass);
        btnPass.setBounds(5, 215, 135, 75);
        btnPass.setLayout(new FlowLayout());
        btnPass.setFont(new Font("Arial", Font.BOLD, 40));
        btnPass.setForeground(whiteColor);
        btnPass.setBackground(fonColor);
        btnPass.setBorder(BorderFactory.createLineBorder(whiteColor));
       if(stage == 1){

           btnPass.setVisible(false);
       }

        btnPass.addActionListener(actionEvent -> {
            cardManager.getPlayersCards().clear();
            cardManager.getCasinoCards().clear();
            cardManager.getFlopCards().clear();
            clearStage();
            valueMenu [0] = 1;
            latch.countDown();
        });

        btnExit = new JButton("Exit");
        panelPlayerField.add(btnExit);
        btnExit.setBounds(5, 215, 135, 75);
        btnExit.setLayout(new FlowLayout());
        btnExit.setFont(new Font("Arial", Font.BOLD, 40));
        btnExit.setForeground(whiteColor);
        btnExit.setBackground(fonColor);
        btnExit.setBorder(BorderFactory.createLineBorder(whiteColor));
        if(stage ==1){
            btnExit.setVisible(true);
        }else {
            btnExit.setVisible(false);
        }

        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dealerCardsList.clear();
                flopCardsList.clear();
                playersCardsList.clear();

                latch.countDown();
            }
        });

        btnPlay = new JButton("PLAY");
        panelPlayerField.add(btnPlay);
        btnPlay.setBounds(5, 5, 135, 90);
        btnPlay.setLayout(new FlowLayout());
        btnPlay.setFont(new Font("Arial", Font.BOLD, 40));
        btnPlay.setForeground(whiteColor);
        btnPlay.setBackground(fonColor);
        btnPlay.setBorder(BorderFactory.createLineBorder(whiteColor));
        btnPlay.setVisible(false);

        btnPlay.addActionListener(actionEvent -> {

            stage++;
            valueMenu [0] = 1;
            latch.countDown();
        });

        ButtonGroup group = new ButtonGroup();

        rbtChip1 = new JRadioButton();
        panelPlayerField.add(rbtChip1);
        rbtChip1.setBounds(10, 301, 60, 50);
        rbtChip1.setBackground(fonColor);
        rbtChip1.setIcon(new ImageIcon(imageManager.getImgChips1()));
        rbtChip1.setHorizontalAlignment(SwingConstants.CENTER);


        rbtChip1.addActionListener(actionEvent -> {
            imgChips = imageManager.getImgChips1();
            imgLtlChips = imageManager.getImgLtlChips1();
            rateCost = 100;
            rbtChip1.setBorder(BorderFactory.createLineBorder(whiteColor));
            rbtChip1.setBorderPainted(true);
            rbtChip2.setBorderPainted(false);
            rbtChip3.setBorderPainted(false);
            rbtChip4.setBorderPainted(false);
            rbtChip5.setBorderPainted(false);

        });


        rbtChip2 = new JRadioButton();
        panelPlayerField.add(rbtChip2);
        rbtChip2.setBounds(10, 351, 60, 50);
        rbtChip2.setBackground(fonColor);
        rbtChip2.setIcon(new ImageIcon(imageManager.getImgChips2()));
        rbtChip2.setHorizontalAlignment(SwingConstants.CENTER);

        rbtChip2.addActionListener(actionEvent -> {
            imgChips = imageManager.getImgChips2();
            imgLtlChips = imageManager.getImgLtlChips2();

            rateCost = 50;

            rbtChip2.setBorder(BorderFactory.createLineBorder(whiteColor));
            rbtChip2.setBorderPainted(true);
            rbtChip1.setBorderPainted(false);
            rbtChip3.setBorderPainted(false);
            rbtChip4.setBorderPainted(false);
            rbtChip5.setBorderPainted(false);
        });

        rbtChip3 = new JRadioButton();
        panelPlayerField.add(rbtChip3);
        rbtChip3.setBounds(10, 401, 60, 50);
        rbtChip3.setBackground(fonColor);
        rbtChip3.setIcon(new ImageIcon(imageManager.getImgChips3()));
        rbtChip3.setHorizontalAlignment(SwingConstants.CENTER);

        rbtChip3.addActionListener(actionEvent -> {
            imgChips = imageManager.getImgChips3();
            imgLtlChips = imageManager.getImgLtlChips3();
            rateCost = 25;

            rbtChip3.setBorder(BorderFactory.createLineBorder(whiteColor));
            rbtChip3.setBorderPainted(true);
            rbtChip1.setBorderPainted(false);
            rbtChip2.setBorderPainted(false);
            rbtChip4.setBorderPainted(false);
            rbtChip5.setBorderPainted(false);
        });

        rbtChip4 = new JRadioButton();
        panelPlayerField.add(rbtChip4);
        rbtChip4.setBounds(10, 451, 60, 50);
        rbtChip4.setBackground(fonColor);
        rbtChip4.setIcon(new ImageIcon(imageManager.getImgChips4()));
        rbtChip4.setHorizontalAlignment(SwingConstants.CENTER);

        rbtChip4.addActionListener(actionEvent -> {
            imgChips = imageManager.getImgChips4();
            imgLtlChips = imageManager.getImgLtlChips4();

            rateCost = 10;

            rbtChip4.setBorder(BorderFactory.createLineBorder(whiteColor));
            rbtChip4.setBorderPainted(true);
            rbtChip1.setBorderPainted(false);
            rbtChip2.setBorderPainted(false);
            rbtChip3.setBorderPainted(false);
            rbtChip5.setBorderPainted(false);
        });

        rbtChip5 = new JRadioButton("", true);
        panelPlayerField.add(rbtChip5);
        rbtChip5.setBounds(10, 501, 60, 50);
        rbtChip5.setBackground(fonColor);
        rbtChip5.setIcon(new ImageIcon(imageManager.getImgChips5()));
        rbtChip5.setHorizontalAlignment(SwingConstants.CENTER);


        rbtChip5.addActionListener(actionEvent -> {
            imgChips = imageManager.getImgChips5();
            imgLtlChips = imageManager.getImgLtlChips5();
            rateCost = 5;

            rbtChip5.setBorder(BorderFactory.createLineBorder(whiteColor));
            rbtChip5.setBorderPainted(true);
            rbtChip1.setBorderPainted(false);
            rbtChip2.setBorderPainted(false);
            rbtChip3.setBorderPainted(false);
            rbtChip4.setBorderPainted(false);
        });

        group.add(rbtChip1);
        group.add(rbtChip2);
        group.add(rbtChip3);
        group.add(rbtChip4);
        group.add(rbtChip5);

        lblCostChips1 = new JLabel();
        panelPlayerField.add(lblCostChips1);
        lblCostChips1.setText("- 100$");
        lblCostChips1.setBounds(75, 301, 70, 50);
        lblCostChips1.setBackground(fonColor);
        lblCostChips1.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips1.setForeground(whiteColor);

        lblCostChips2 = new JLabel();
        panelPlayerField.add(lblCostChips2);
        lblCostChips2.setText("- 50$");
        lblCostChips2.setBounds(75, 351, 50, 50);
        lblCostChips2.setBackground(fonColor);
        lblCostChips2.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips2.setForeground(whiteColor);

        lblCostChips3 = new JLabel();
        panelPlayerField.add(lblCostChips3);
        lblCostChips3.setText("- 25$");
        lblCostChips3.setBounds(75, 401, 50, 50);
        lblCostChips3.setBackground(fonColor);
        lblCostChips3.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips3.setForeground(whiteColor);

        lblCostChips4 = new JLabel();
        panelPlayerField.add(lblCostChips4);
        lblCostChips4.setText("- 10$");
        lblCostChips4.setBounds(75, 451, 50, 50);
        lblCostChips4.setBackground(fonColor);
        lblCostChips4.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips4.setForeground(whiteColor);

        lblCostChips5 = new JLabel();
        panelPlayerField.add(lblCostChips5);
        lblCostChips5.setText("- 5$");
        lblCostChips5.setBounds(75, 501, 50, 50);
        lblCostChips5.setBackground(fonColor);
        lblCostChips5.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips5.setForeground(whiteColor);

        frame.setVisible(true);
    }

    public void createPanelPlayerLabels() {

        panelPlayerLabels = new JPanel();
        frame.add(panelPlayerLabels);
        panelPlayerLabels.setBounds(650, 452, 335, 451);
        panelPlayerLabels.setBackground(fonColor);
        panelPlayerLabels.setLayout(null);

        lblAllCash = new JLabel();
        lblAllRateCost = new JLabel();

        lblAllCash = new JLabel();
        panelPlayerLabels.add(lblAllCash);

        lblAllCash.setText("$ " + moneyManager.getAllCash() + " $");
        lblAllCash.setForeground(whiteColor);
        lblAllCash.setFont(new Font("Arial", Font.ITALIC, 40));

        lblAllCash.setBackground(fonColor);
        lblAllCash.setOpaque(true);
        lblAllCash.setBounds(130, 0, 200, 40);

        lblAllRateCost = new JLabel();
        panelPlayerLabels.add(lblAllRateCost);
        lblAllRateCost.setText("Bank: " + bank + " $");
        lblAllRateCost.setForeground(whiteColor);
        lblAllRateCost.setHorizontalTextPosition(SwingConstants.CENTER);
        lblAllRateCost.setFont(new Font("Arial", Font.ITALIC, 20));

        lblAllRateCost.setBackground(fonColor);
        lblAllRateCost.setOpaque(true);
        lblAllRateCost.setBounds(145, 35, 200, 40);

        frame.setVisible(true);
    }

    public void clearStage() {

        frameRestart();
        stage = 1;
        flopCardsList.clear();
        dealerCardsList.clear();
        playersCardsList.clear();
        bank = 0;

        round++;
        System.out.println("Round: " + round);
    }

    public void updateFieldPanels(int allCash, int allRateCost) {
        lblAllCash.setText("$ " + allCash + " $");
        lblAllRateCost.setText("Bank: " + bank + " $");


        if ((allCash - 100) < (bank + 100)) {
            rbtChip1.setEnabled(false);
            rbtChip1.setVisible(false);
            lblCostChips1.setVisible(false);
        }

        if ((allCash - 50) < (bank + 50 )) {
            rbtChip2.setEnabled(false);
            rbtChip2.setVisible(false);
            lblCostChips2.setVisible(false);
        }

        if ((allCash - 25) < (bank + 25)) {
            rbtChip3.setEnabled(false);
            rbtChip3.setVisible(false);
            lblCostChips3.setVisible(false);
        }

        if ((allCash - 10) < (bank + 10)) {
            rbtChip4.setEnabled(false);
            rbtChip4.setVisible(false);
            lblCostChips4.setVisible(false);

        }

        if ((allCash - 5) < (bank + 5)) {
            rbtChip5.setEnabled(false);
            rbtChip5.setVisible(false);
            lblCostChips5.setVisible(false);

        }
        frame.setVisible(true);
    }
}
