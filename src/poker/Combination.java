package poker;

import java.util.List;

public class Combination {

    private boolean royalFlush;
    private int straightFlush;
    private int fourOfAKing;
    private List<Integer> fullHouse;
    private List<Integer> flush;
    private List<Integer> straight;
    private int threeOfAKing;
    private List<Integer> twoPair;
    private int pair;
    private List<Integer> highCard;

    public Combination(boolean royalFlush, int straightFlush, int fourOfAKing, List<Integer> fullHouse, List<Integer> flush,
                       List<Integer> straight, int threeOfAKing, List<Integer> twoPair, int pair, List<Integer> highCard) {

        this.royalFlush = royalFlush;
        this.straightFlush = straightFlush;
        this.fourOfAKing = fourOfAKing;
        this.fullHouse = fullHouse;
        this.flush = flush;
        this.straight = straight;
        this.threeOfAKing = threeOfAKing;
        this.twoPair = twoPair;
        this.pair = pair;
        this.highCard = highCard;
    }

    public boolean isRoyalFlush() {
        return royalFlush;
    }

    public void setRoyalFlush(boolean royalFlush) {
        this.royalFlush = royalFlush;
    }

    public int getStraightFlush() {
        return straightFlush;
    }

    public void setStraightFlush(int straightFlush) {
        this.straightFlush = straightFlush;
    }

    public int getFourOfAKing() {
        return fourOfAKing;
    }

    public void setFourOfAKing(int fourOfAKing) {
        this.fourOfAKing = fourOfAKing;
    }

    public List<Integer> getFullHouse() {
        return fullHouse;
    }

    public void setFullHouse(List<Integer> fullHouse) {
        this.fullHouse = fullHouse;
    }

    public List<Integer> getFlush() {
        return flush;
    }

    public void setFlush(List<Integer> flush) {
        this.flush = flush;
    }

    public List<Integer> getStraight() {
        return straight;
    }

    public void setStraight(List<Integer> straight) {
        this.straight = straight;
    }

    public int getThreeOfAKing() {
        return threeOfAKing;
    }

    public void setThreeOfAKing(int threeOfAKing) {
        this.threeOfAKing = threeOfAKing;
    }

    public List<Integer> getTwoPair() {
        return twoPair;
    }

    public void setTwoPair(List<Integer> twoPair) {
        this.twoPair = twoPair;
    }

    public int getPair() {
        return pair;
    }

    public void setPair(int pair) {
        this.pair = pair;
    }

    public List<Integer> getHighCard() {
        return highCard;
    }

    public void setHighCard(List<Integer> highCard) {
        this.highCard = highCard;
    }

    @Override
    public String toString() {
        return "royalFlush=" + royalFlush +
                ", straightFlush=" + straightFlush +
                ", fourOfAKing=" + fourOfAKing +
                ", fullHouse=" + fullHouse +
                ", flush=" + flush +
                ", straight=" + straight +
                ", threeOfAKing=" + threeOfAKing +
                ", twoPair=" + twoPair +
                ", pair=" + pair +
                ", highCard=" + highCard +
                '}';
    }
}
