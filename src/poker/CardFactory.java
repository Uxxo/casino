package poker;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class CardFactory {

    Image image;

    public List<Card> createDeckCard() {
        List<Card> deckCards = new ArrayList<Card>();

        for (int suit = 1; suit < 5; suit++) {
            for (int faceValue = 1; faceValue < 14; faceValue++) {
                image = new ImageIcon("res/cards/" + suit + "x" + faceValue + ".png").getImage();
                Card card = new Card(suit, faceValue, image);
                deckCards.add(card);
            }
        }
        return deckCards;
    }
}
