package roulette;

import java.awt.*;

public enum NumberColor {



    RED(new java.awt.Color(255,0,0)),
    BLACK(new java.awt.Color(0,0,0)),
    ZERO(new java.awt.Color(15,57,0));

    java.awt.Color color;

    NumberColor(java.awt.Color color){
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
