package roulette;

import money.MoneyManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.concurrent.CountDownLatch;



public class PlayRoulette {

    NumbersManager numbersManager = new NumbersManager();
    MoneyManager moneyManager;
    ImageManager imageManager = new ImageManager();



    private Graphics2D g;
    private BufferedImage bufferedImage;


    private Number result;

    private int allCash;

    private boolean doBets;
    private int rateCost;
    private int allRateCost;
    final int[] valueMenu = {0};
    Image imgChips;
    Image imgLtlChips;
    Image imgRoulette;
    Image imgRouletteCentre;



    JFrame frame;
    JPanel panelLastNumbers;
    JPanel panelRoulette;
    JPanel panelPlayingField;

    JPanel panelPlayerField;
    JPanel panelRotationRoulette;


    JLabel lblImageRoulette;
    JLabel lblImageRouletteCentre;
    JLabel lblPanelPlayingField;

    JLabel lblAllRateCost;
    JLabel lblAllCash;

//    JLabel lblLastNumbers;

    JButton btnStartRoulette;
    JButton btnOut;

    JRadioButton rbtChip1;
    JRadioButton rbtChip2;
    JRadioButton rbtChip3;
    JRadioButton rbtChip4;
    JRadioButton rbtChip5;

    JLabel lblCostChips1;
    JLabel lblCostChips2;
    JLabel lblCostChips3;
    JLabel lblCostChips4;
    JLabel lblCostChips5;

    JLabel lblRotationRoulette;

    JLabel lblMessage;
    JLabel lblMessageSum;

    Color fonColor = new Color(16, 53, 9);
    Color whiteColor = new Color(255, 255, 255);
    Color winColor = new Color(255, 199, 0);


    public PlayRoulette(JFrame frame, MoneyManager moneyManager) {
        this.frame = frame;
        this.moneyManager = moneyManager;
        rateCost = 5;

        frame.setTitle("Roulette");
        imgChips = imageManager.getImgChips5();
        imgLtlChips = imageManager.getImgLtlChips5();


        frame.setTitle("Roulette");
        frame.revalidate();

    }




    public void frameRestart() {

        frame.remove(panelLastNumbers);
        frame.remove(panelPlayerField);
        frame.remove(panelPlayingField);
        frame.remove(panelRoulette);
        frame.remove(panelRotationRoulette);

        frame.revalidate();
        frame.repaint();


    }

    public void createPanelLastNumbers() {
        Font numbersFont = new Font("Arial", Font.ITALIC, 18);

        List<Number> numberList = numbersManager.getResultList();

        panelLastNumbers = new JPanel();
        frame.add(panelLastNumbers);
        panelLastNumbers.setBounds(0, 0, 600, 40);
        panelLastNumbers.setBackground(fonColor);
        panelLastNumbers.setLayout(null);

        int counter = 0;
        int x = panelLastNumbers.getWidth() - 40;

        if (numberList.size() > 0) {
            for (int i = 0; i < numberList.size(); i++) {
                Number number = numberList.get(i);


                JLabel lblLastNumbers = new JLabel(String.valueOf(number.getNumber()));

                panelLastNumbers.add(lblLastNumbers);
                lblLastNumbers.setOpaque(true);
                lblLastNumbers.setBounds(x, 5, 30, 30);
                lblLastNumbers.setBackground(number.getNumberColor().getColor());
                lblLastNumbers.setFont(numbersFont);
                lblLastNumbers.setForeground(whiteColor);
                lblLastNumbers.setHorizontalAlignment(SwingConstants.CENTER);
                lblLastNumbers.setVerticalTextPosition(SwingConstants.CENTER);
                lblLastNumbers.setBorder(BorderFactory.createLineBorder(whiteColor));

                x -= 35;
                counter++;

                if (counter == 10) {
                    return;
                }
            }
        }


//        frame.setVisible(true);
    }

    public void createPanelRoulette() {


        imgRoulette = imageManager.getImgRoulette();
        imgRouletteCentre = imageManager.getImgRouletteCentre();

        panelRoulette = new JPanel();
        frame.add(panelRoulette);
        panelRoulette.setBounds(600, 0, 400, 400);
        panelRoulette.setBackground(fonColor);
        panelRoulette.setLayout(null);


        lblImageRoulette = new JLabel(new ImageIcon(imageManager.getImgRoulette()));
        panelRoulette.add(lblImageRoulette);
        lblImageRoulette.setBounds(0, 0, 400, 400);

        lblImageRouletteCentre = new JLabel(new ImageIcon(imgRouletteCentre));
        panelRoulette.add(lblImageRouletteCentre);
        lblImageRouletteCentre.setBounds(0, 0, 400, 400);


    }

    public void createPanelPlayingField() {
        panelPlayingField = new JPanel();
        frame.add(panelPlayingField);
        panelPlayingField.setBounds(0, 40, 600, 360);
        panelPlayingField.setBackground(fonColor);
        panelPlayingField.setLayout(null);

        lblPanelPlayingField = new JLabel(new ImageIcon(imageManager.getImgPlayingField()));

        Map<Integer, Integer> bettingMap = new HashMap<>();

        JButton button0 = new JButton();
        panelPlayingField.add(button0);

        button0.setBounds(20, 50, 40, 172);
        button0.setContentAreaFilled(false);
        button0.setOpaque(false);
        button0.setLayout(new FlowLayout());
        button0.setRolloverEnabled(false);
        button0.setFocusPainted(false);


        if (moneyManager.getAllCash() <= 0) {
            button0.setEnabled(false);
        }

        button0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                allCash = moneyManager.getAllCash();
                if (allCash >= rateCost && doBets) {

                    numbersManager.setBettingMap(0, rateCost);
                    allRateCost += rateCost;

                    allCash -= rateCost;
                    moneyManager.setAllCash(allCash);

                    JLabel lblIconChips = new JLabel(new ImageIcon(imgLtlChips));
                    lblIconChips.setOpaque(true);
                    lblIconChips.setBackground(fonColor);

                    button0.add(lblIconChips);
                    button0.setOpaque(true);
                    button0.setBackground(null);
                    button0.setContentAreaFilled(true);
                    frame.setVisible(true);
                }
                updatePlayerFieldPanel(allCash, allRateCost);
//                frame.setVisible(true);
            }
        });

        int x = 64;
        int y = 165;
        int counter = 0;
        for (int i = 1; i < 37; i++) {
            JButton button = new JButton();
            panelPlayingField.add(button);

            if (counter == 3) {
                x = x + 41;
                y = 165;
                counter = 0;
            }
            button.setBounds(x, y, 38, 57);
            button.setContentAreaFilled(false);
            button.setLayout(new FlowLayout());
            button.setRolloverEnabled(false);

            int finalI = i;
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    allCash = moneyManager.getAllCash();
                    if (allCash >= rateCost && doBets) {
                        numbersManager.setBettingMap(finalI, rateCost);
                        allRateCost += rateCost;

                        allCash -= rateCost;
                        moneyManager.setAllCash(allCash);

                        JLabel lblIconChips = new JLabel(new ImageIcon(imgLtlChips));
                        lblIconChips.setBackground(fonColor);
                        lblIconChips.setSize(10, 10);
                        lblIconChips.setOpaque(true);

                        button.add(lblIconChips);
                        button.setBackground(null);
                        button.setContentAreaFilled(true);
                    }

                    updatePlayerFieldPanel(allCash, allRateCost);
//                    frame.setVisible(true);
                }
            });
            y = y - 57;
            counter++;
        }

        x = 64;
        y = 229;

        for (int i = 51; i < 54; i++) {
            JButton button = new JButton();
            panelPlayingField.add(button);

            button.setBounds(x, y, 159, 38);
            button.setContentAreaFilled(false);
            button.setLayout(new GridLayout(1, 0));
            button.setRolloverEnabled(false);

            x = x + 165;

            int finalI = i;
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    int allCash = moneyManager.getAllCash();
                    if (allCash >= rateCost && doBets) {

                        numbersManager.setBettingMap(finalI, rateCost);

                        allRateCost += rateCost;

                        allCash -= rateCost;
                        moneyManager.setAllCash(allCash);

                        JLabel lblIconChips = new JLabel(new ImageIcon(imgLtlChips));
                        lblIconChips.setBackground(fonColor);
                        lblIconChips.setOpaque(true);

                        button.add(lblIconChips);
                        button.setBackground(null);
                        button.setContentAreaFilled(true);
                    }
                    updatePlayerFieldPanel(allCash, allRateCost);
//                    frame.setVisible(true);
                }
            });
        }

        x = 558;
        y = 165;

        for (int i = 71; i < 74; i++) {
            JButton button = new JButton();
            panelPlayingField.add(button);

            button.setBounds(x, y, 38, 57);
            button.setContentAreaFilled(false);
            button.setLayout(new FlowLayout());
            button.setRolloverEnabled(false);

            y = y - 57;


            int finalI = i;
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    int allCash = moneyManager.getAllCash();
                    if (allCash >= rateCost && doBets) {
                        numbersManager.setBettingMap(finalI, rateCost);

                        allRateCost += rateCost;

                        allCash -= rateCost;
                        moneyManager.setAllCash(allCash);

                        JLabel lblIconChips = new JLabel(new ImageIcon(imgLtlChips));
                        lblIconChips.setBackground(fonColor);
                        lblIconChips.setOpaque(true);

                        button.add(lblIconChips);
                        button.setBackground(null);
                        button.setContentAreaFilled(true);
                    }
                    updatePlayerFieldPanel(allCash, allRateCost);
//                    frame.setVisible(true);
                }
            });
        }

        x = 64;
        y = 272;

        for (int i = 61; i < 67; i++) {
            JButton button = new JButton();
            panelPlayingField.add(button);

            button.setBounds(x, y, 78, 40);
            button.setContentAreaFilled(false);
            button.setLayout(new GridLayout(1, 0));
            button.setRolloverEnabled(false);

            x = x + 83;

            int finalI = i;
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    int allCash = moneyManager.getAllCash();
                    if (allCash >= rateCost && doBets) {

                        numbersManager.setBettingMap(finalI, rateCost);

                        allRateCost += rateCost;

                        allCash -= rateCost;
                        moneyManager.setAllCash(allCash);

                        JLabel lblIconChips = new JLabel(new ImageIcon(imgLtlChips));
                        lblIconChips.setBackground(fonColor);
                        lblIconChips.setOpaque(true);

                        button.add(lblIconChips);
                        button.setBackground(null);
                        button.setContentAreaFilled(true);
                    }

                    updatePlayerFieldPanel(allCash, allRateCost);
//                    frame.setVisible(true);
                }
            });

        }

        panelPlayingField.add(lblPanelPlayingField);

        lblPanelPlayingField.setBounds(0, 0, 600, 360);
//        frame.setVisible(true);
    }

    public void createPanelRotationRoulette() {

        panelRotationRoulette = new JPanel();
        frame.add(panelRotationRoulette);
        panelRotationRoulette.setBounds(630, 150, 100, 100);

        panelRotationRoulette.setLayout(null);
        panelRotationRoulette.setVisible(false);
        lblRotationRoulette = new JLabel();
//        frame.setVisible(true);
    }

    public void createPanelPlayerField(CountDownLatch latch) {

        panelPlayerField = new JPanel();
        frame.add(panelPlayerField);
        panelPlayerField.setBounds(0, 400, 1000, 300);
        panelPlayerField.setBackground(fonColor);
        panelPlayerField.setLayout(null);

//        btnStartRoulette = new JButton("Go!");

        lblAllCash = new JLabel();
        lblAllRateCost = new JLabel();

        lblMessage = new JLabel();
        lblMessageSum = new JLabel();

        lblAllCash = new JLabel();
        panelPlayerField.add(lblAllCash);


        lblAllCash.setText("$ " + moneyManager.getAllCash() + " $");
        lblAllCash.setForeground(whiteColor);
        lblAllCash.setFont(new Font("Arial", Font.ITALIC, 40));

        lblAllCash.setBackground(fonColor);
        lblAllCash.setOpaque(true);
        lblAllCash.setBounds(700, 0, 200, 40);


        lblAllRateCost = new JLabel();
        panelPlayerField.add(lblAllRateCost);
        lblAllRateCost.setText("На столе: " + allRateCost + " $");
        lblAllRateCost.setForeground(whiteColor);
        lblAllRateCost.setFont(new Font("Arial", Font.ITALIC, 20));

        lblAllRateCost.setBackground(fonColor);
        lblAllRateCost.setOpaque(true);
        lblAllRateCost.setBounds(700, 50, 200, 40);


        ButtonGroup group = new ButtonGroup();

        rbtChip1 = new JRadioButton();
        panelPlayerField.add(rbtChip1);
        rbtChip1.setBounds(10, 0, 60, 50);
        rbtChip1.setBackground(fonColor);
        rbtChip1.setIcon(new ImageIcon(imageManager.getImgChips1()));
        rbtChip1.setHorizontalAlignment(SwingConstants.CENTER);


        rbtChip1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                imgChips = imageManager.getImgChips1();
                imgLtlChips = imageManager.getImgLtlChips1();
                rateCost = 100;
                rbtChip1.setBorder(BorderFactory.createLineBorder(whiteColor));
                rbtChip1.setBorderPainted(true);
                rbtChip2.setBorderPainted(false);
                rbtChip3.setBorderPainted(false);
                rbtChip4.setBorderPainted(false);
                rbtChip5.setBorderPainted(false);

            }
        });


        rbtChip2 = new JRadioButton();
        panelPlayerField.add(rbtChip2);
        rbtChip2.setBounds(10, 50, 60, 50);
        rbtChip2.setBackground(fonColor);
        rbtChip2.setIcon(new ImageIcon(imageManager.getImgChips2()));
        rbtChip2.setHorizontalAlignment(SwingConstants.CENTER);

        rbtChip2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                imgChips = imageManager.getImgChips2();
                imgLtlChips = imageManager.getImgLtlChips2();

                rateCost = 50;

                rbtChip2.setBorder(BorderFactory.createLineBorder(whiteColor));
                rbtChip2.setBorderPainted(true);
                rbtChip1.setBorderPainted(false);
                rbtChip3.setBorderPainted(false);
                rbtChip4.setBorderPainted(false);
                rbtChip5.setBorderPainted(false);
            }
        });

        rbtChip3 = new JRadioButton();
        panelPlayerField.add(rbtChip3);
        rbtChip3.setBounds(10, 100, 60, 50);
        rbtChip3.setBackground(fonColor);
        rbtChip3.setIcon(new ImageIcon(imageManager.getImgChips3()));
        rbtChip3.setHorizontalAlignment(SwingConstants.CENTER);

        rbtChip3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                imgChips = imageManager.getImgChips3();
                imgLtlChips = imageManager.getImgLtlChips3();
                rateCost = 25;

                rbtChip3.setBorder(BorderFactory.createLineBorder(whiteColor));
                rbtChip3.setBorderPainted(true);
                rbtChip1.setBorderPainted(false);
                rbtChip2.setBorderPainted(false);
                rbtChip4.setBorderPainted(false);
                rbtChip5.setBorderPainted(false);
            }
        });

        rbtChip4 = new JRadioButton();
        panelPlayerField.add(rbtChip4);
        rbtChip4.setBounds(10, 150, 60, 50);
        rbtChip4.setBackground(fonColor);
        rbtChip4.setIcon(new ImageIcon(imageManager.getImgChips4()));
        rbtChip4.setHorizontalAlignment(SwingConstants.CENTER);

        rbtChip4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                imgChips = imageManager.getImgChips4();
                imgLtlChips = imageManager.getImgLtlChips4();

                rateCost = 10;

                rbtChip4.setBorder(BorderFactory.createLineBorder(whiteColor));
                rbtChip4.setBorderPainted(true);
                rbtChip1.setBorderPainted(false);
                rbtChip2.setBorderPainted(false);
                rbtChip3.setBorderPainted(false);
                rbtChip5.setBorderPainted(false);
            }
        });

        rbtChip5 = new JRadioButton("", true);
        panelPlayerField.add(rbtChip5);
        rbtChip5.setBounds(10, 200, 60, 50);
        rbtChip5.setBackground(fonColor);
        rbtChip5.setIcon(new ImageIcon(imageManager.getImgChips5()));
        rbtChip5.setHorizontalAlignment(SwingConstants.CENTER);


        rbtChip5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                imgChips = imageManager.getImgChips5();
                imgLtlChips = imageManager.getImgLtlChips5();
                rateCost = 5;

                rbtChip5.setBorder(BorderFactory.createLineBorder(whiteColor));
                rbtChip5.setBorderPainted(true);
                rbtChip1.setBorderPainted(false);
                rbtChip2.setBorderPainted(false);
                rbtChip3.setBorderPainted(false);
                rbtChip4.setBorderPainted(false);
            }
        });

        group.add(rbtChip1);
        group.add(rbtChip2);
        group.add(rbtChip3);
        group.add(rbtChip4);
        group.add(rbtChip5);

        lblCostChips1 = new JLabel();
        panelPlayerField.add(lblCostChips1);
        lblCostChips1.setText("- 100$");
        lblCostChips1.setBounds(75, 0, 70, 50);
        lblCostChips1.setBackground(fonColor);
        lblCostChips1.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips1.setForeground(whiteColor);

        lblCostChips2 = new JLabel();
        panelPlayerField.add(lblCostChips2);
        lblCostChips2.setText("- 50$");
        lblCostChips2.setBounds(75, 50, 50, 50);
        lblCostChips2.setBackground(fonColor);
        lblCostChips2.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips2.setForeground(whiteColor);

        lblCostChips3 = new JLabel();
        panelPlayerField.add(lblCostChips3);
        lblCostChips3.setText("- 25$");
        lblCostChips3.setBounds(75, 100, 50, 50);
        lblCostChips3.setBackground(fonColor);
        lblCostChips3.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips3.setForeground(whiteColor);

        lblCostChips4 = new JLabel();
        panelPlayerField.add(lblCostChips4);
        lblCostChips4.setText("- 10$");
        lblCostChips4.setBounds(75, 150, 50, 50);
        lblCostChips4.setBackground(fonColor);
        lblCostChips4.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips4.setForeground(whiteColor);

        lblCostChips5 = new JLabel();
        panelPlayerField.add(lblCostChips5);
        lblCostChips5.setText("- 5$");
        lblCostChips5.setBounds(75, 200, 50, 50);
        lblCostChips5.setBackground(fonColor);
        lblCostChips5.setFont(new Font("Arial", Font.ITALIC, 20));
        lblCostChips5.setForeground(whiteColor);


        btnStartRoulette = new JButton("Go!");
        panelPlayerField.add(btnStartRoulette);
        btnStartRoulette.setBounds(330, 200, 300, 50);
        btnStartRoulette.setBackground(null);
        btnStartRoulette.setForeground(whiteColor);
        btnStartRoulette.setFont(new Font("Arial", Font.BOLD, 30));
        btnStartRoulette.setVisible(false);

        btnStartRoulette.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                doBets = false;
                btnStartRoulette.setVisible(false);
                valueMenu[0] = 1;
                latch.countDown();
            }
        });

        btnOut = new JButton("Exit");
        panelPlayerField.add(btnOut);
        btnOut.setBounds(330, 200, 300, 50);
        btnOut.setBackground(null);
        btnOut.setForeground(whiteColor);
        btnOut.setFont(new Font("Arial", Font.BOLD, 30));
        btnOut.setBorderPainted(false);
        btnOut.setFocusPainted(false);
        btnOut.setVisible(true);

        btnOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                latch.countDown();
            }
        });

        updatePlayerFieldPanel(allCash, allRateCost);

//        frame.setVisible(true);

    }

    public int createGamePlay() {
        CountDownLatch latch = new CountDownLatch(1);

        valueMenu[0] = 0;

        allCash = moneyManager.getAllCash();
        doBets = true;
        allRateCost = 0;

        createPanelLastNumbers();
        createPanelPlayingField();
        createPanelRoulette();


        createPanelPlayerField(latch);
        createPanelRotationRoulette();
        createPanelRoulette();

        frame.setVisible(true);

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (valueMenu[0] == 0){
            frameRestart();
        }
        return valueMenu[0];
    }

    public void updatePlayerFieldPanel(int allCash, int allRateCost) {
        lblAllCash.setText("$ " + allCash + " $");
        lblAllRateCost.setText("На столе: " + allRateCost + " $");

        if (allRateCost > 0 && doBets) {
            btnStartRoulette.setVisible(true);
            btnOut.setVisible(false);
        }
        if (allCash < 100) {
            rbtChip1.setEnabled(false);
            rbtChip1.setVisible(false);
            lblCostChips1.setVisible(false);
        }
        if (allCash < 50) {
            rbtChip2.setEnabled(false);
            rbtChip2.setVisible(false);
            lblCostChips2.setVisible(false);
        }
        if (allCash < 25) {
            rbtChip3.setEnabled(false);
            rbtChip3.setVisible(false);
            lblCostChips3.setVisible(false);
        }
        if (allCash < 10) {
            rbtChip4.setEnabled(false);
            rbtChip4.setVisible(false);
            lblCostChips4.setVisible(false);

        }
        if (allCash < 5) {
            rbtChip5.setEnabled(false);
            rbtChip5.setVisible(false);
            lblCostChips5.setVisible(false);

        }


//        frame.setVisible(true);
    }

    public void checkBetsMessage() {


        panelPlayerField.add(lblMessage);
        lblMessage.setBounds(250, 0, 350, 100);
        lblMessage.setHorizontalAlignment(SwingConstants.CENTER);
        lblMessage.setForeground(winColor);
        lblMessage.setVisible(true);

        lblMessageSum = new JLabel();
        panelPlayerField.add(lblMessageSum);
        lblMessageSum.setBounds(250, 100, 350, 100);
        lblMessageSum.setHorizontalAlignment(SwingConstants.CENTER);
        lblMessageSum.setForeground(winColor);
        lblMessageSum.setVisible(true);



        int win = numbersManager.checkBets(result, moneyManager);

        if (win > 0) {
            lblMessage.setText("Вы выиграли");
            lblMessage.setFont(new Font("Arial", Font.BOLD, 40));
            lblMessageSum.setText(String.valueOf(win) + "$");
            lblMessageSum.setForeground(winColor);
            lblMessageSum.setFont(new Font("Arial", Font.ITALIC, 55));
            lblMessage.setForeground(winColor);
        } else {
            lblMessage.setText("Ваша ставка");
            lblMessage.setFont(new Font("Arial", Font.BOLD, 40));
            lblMessage.setForeground(whiteColor);
            lblMessageSum.setText("не сыграла");
            lblMessageSum.setFont(new Font("Arial", Font.BOLD, 40));
            lblMessageSum.setForeground(whiteColor);

        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void rotationRoulette() {

        double angle = -0.0972972972972972972972972972972972972972972972972972972972972972972972972972972972972972972972972;

        Font font = new Font("Arial", 1, 70);

        Number[] rouletteNumbersArray = numbersManager.getRouletteNumbersArray();


        panelRotationRoulette.setVisible(true);
        panelRotationRoulette.add(lblRotationRoulette);

        lblRotationRoulette.setBounds(0, 0, 100, 100);
        lblRotationRoulette.setBorder(BorderFactory.createLineBorder(whiteColor));


        int rotation = (int) (Math.random() * 38 + 148);


        int pause = 40;


        RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        bufferedImage = new BufferedImage(imgRouletteCentre.getWidth(null),
                imgRouletteCentre.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bufferedImage.createGraphics();

        AffineTransform nextForm = g.getTransform();

        for (int i = 0; i < rouletteNumbersArray.length; i++) {
            Number number = rouletteNumbersArray[i];


            nextForm.rotate(angle, 119, 117);
            g.setRenderingHints(qualityHints);
            g.setTransform(nextForm);
            g.drawImage(imgRouletteCentre, 0, 0, null);

            lblImageRouletteCentre.setIcon(new ImageIcon(bufferedImage));




            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            lblRotationRoulette.setBackground(new Color(number.getNumberColor().getColor().getRGB()));
            lblRotationRoulette.setOpaque(true);
            lblRotationRoulette.setText(String.valueOf(number.getNumber()));
            lblRotationRoulette.setFont(font);
            lblRotationRoulette.setForeground(whiteColor);
            lblRotationRoulette.setHorizontalAlignment(JLabel.CENTER);


            try {
                Thread.sleep(pause);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (rotation < 75) {
                pause = pause + pause / 25;
            }

            if (rotation == 0) {
                lblRotationRoulette.setBorder(BorderFactory.createLineBorder(new Color(0xFF9700)));
                result = rouletteNumbersArray[i];
                numbersManager.setResultList(result);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return;
            }
            if (i == rouletteNumbersArray.length - 1) {
                i = -1;
            }
            rotation--;
        }

    }
}

