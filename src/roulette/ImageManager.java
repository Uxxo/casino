package roulette;

import javax.swing.*;
import java.awt.*;

public class ImageManager {

   private Image imgRoulette = new ImageIcon("res/rouletteRing.png").getImage();



   private Image imgRouletteCentre = new ImageIcon("res/rouletteCentre.png").getImage();

   private Image imgPlayingField = new ImageIcon("res/ruletstol.png").getImage();

   private Image imgChips1 = new ImageIcon("res/chips1.png").getImage();
   private Image imgChips2 = new ImageIcon("res/chips2.png").getImage();
   private Image imgChips3 = new ImageIcon("res/chips3.png").getImage();
   private Image imgChips4 = new ImageIcon("res/chips4.png").getImage();
   private Image imgChips5 = new ImageIcon("res/chips5.png").getImage();

   private Image imgLtlChips1 = new ImageIcon("res/ltlchips1.png").getImage();
   private Image imgLtlChips2 = new ImageIcon("res/ltlchips2.png").getImage();
   private Image imgLtlChips3 = new ImageIcon("res/ltlchips3.png").getImage();
   private Image imgLtlChips4 = new ImageIcon("res/ltlchips4.png").getImage();
   private Image imgLtlChips5 = new ImageIcon("res/ltlchips5.png").getImage();

   private Image imgBackSideCard = new ImageIcon("res/cards/backSide.png").getImage();

   private Image imgBackgroundMenu = new ImageIcon("res/cards/StartMenu.png").getImage();



    public Image getImgRoulette() {
        return imgRoulette;
    }

    public Image getImgRouletteCentre() {
        return imgRouletteCentre;
    }

    public Image getImgPlayingField() {
        return imgPlayingField;
    }

    public Image getImgChips1() {
        return imgChips1;
    }

    public Image getImgChips2() {
        return imgChips2;
    }

    public Image getImgChips3() {
        return imgChips3;
    }

    public Image getImgChips4() {
        return imgChips4;
    }

    public Image getImgChips5() {
        return imgChips5;
    }

    public Image getImgLtlChips1() {
        return imgLtlChips1;
    }

    public Image getImgLtlChips2() {
        return imgLtlChips2;
    }

    public Image getImgLtlChips3() {
        return imgLtlChips3;
    }

    public Image getImgLtlChips4() {
        return imgLtlChips4;
    }

    public Image getImgLtlChips5() {
        return imgLtlChips5;
    }

    public Image getImgBackSideCard() {
        return imgBackSideCard;
    }

    public Image getImgBackgroundMenu() {
        return imgBackgroundMenu;
    }
}
