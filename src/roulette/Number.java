package roulette;

public class Number {

    private int number;
    NumberColor numberColor;


    Number(int number, NumberColor numberColor){
        this.number = number;
        this.numberColor = numberColor;
    }

    public int getNumber() {
        return number;
    }

    public NumberColor getNumberColor() {
        return numberColor;
    }
}
