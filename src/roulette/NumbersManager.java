package roulette;

import money.MoneyManager;

import java.util.*;

public class NumbersManager {

    private List<Integer> first = Arrays.asList(1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34);
    private List<Integer> second = Arrays.asList(2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35);
    private List<Integer> third = Arrays.asList(3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36);

    //    private List<Number> resultList = Arrays.asList(new Number(32, NumberColor.RED), new Number(15, NumberColor.BLACK));
    private List<Number> resultList = new LinkedList<>();

    private Map<Integer, Integer> bettingMap = new HashMap<>();

    private Number[] rouletteNumbersArray = new Number[]{new Number(0, NumberColor.ZERO),
            new Number(32, NumberColor.RED), new Number(15, NumberColor.BLACK),
            new Number(19, NumberColor.RED), new Number(4, NumberColor.BLACK),
            new Number(21, NumberColor.RED), new Number(2, NumberColor.BLACK),
            new Number(25, NumberColor.RED), new Number(17, NumberColor.BLACK),
            new Number(34, NumberColor.RED), new Number(6, NumberColor.BLACK),
            new Number(27, NumberColor.RED), new Number(13, NumberColor.BLACK),
            new Number(36, NumberColor.RED), new Number(11, NumberColor.BLACK),
            new Number(30, NumberColor.RED), new Number(8, NumberColor.BLACK),
            new Number(23, NumberColor.RED), new Number(10, NumberColor.BLACK),
            new Number(5, NumberColor.RED), new Number(24, NumberColor.BLACK),
            new Number(16, NumberColor.RED), new Number(33, NumberColor.BLACK),
            new Number(1, NumberColor.RED), new Number(20, NumberColor.BLACK),
            new Number(14, NumberColor.RED), new Number(31, NumberColor.BLACK),
            new Number(9, NumberColor.RED), new Number(22, NumberColor.BLACK),
            new Number(18, NumberColor.RED), new Number(29, NumberColor.BLACK),
            new Number(7, NumberColor.RED), new Number(28, NumberColor.BLACK),
            new Number(12, NumberColor.RED), new Number(35, NumberColor.BLACK),
            new Number(3, NumberColor.RED), new Number(26, NumberColor.BLACK),};


    public void setBettingMap(Integer number, int cost) {
        Set<Integer> keySet = bettingMap.keySet();

        if (keySet.contains(number)) {
            int allCostRate = bettingMap.get(number) + cost;
            bettingMap.put(number, allCostRate);
        } else {
            bettingMap.put(number, cost);
        }
    }

    public void setResultList(Number result) {
        resultList.add(0, result);

    }

    public int checkBets(Number result, MoneyManager moneyManager) {
        int value;
        int cash = 0;
        int win = 0;
        Set<Integer> keySet = bettingMap.keySet();
        if (keySet.contains(result.getNumber())) {
            int bettingSum = bettingMap.get(result.getNumber());
            value = bettingSum * 36;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(63) && result.getNumberColor() == NumberColor.RED) { // красное
            int bettingSum = bettingMap.get(63);
            value = bettingSum * 2;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(64) && result.getNumberColor() == NumberColor.BLACK) { // черное
            int bettingSum = bettingMap.get(64);
            value = bettingSum * 2;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(61) && 0 < result.getNumber() && result.getNumber() < 19) { // малые 1-18
            int bettingSum = bettingMap.get(61);
            value = bettingSum * 2;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(66) && 18 < result.getNumber() && result.getNumber() < 37) { // малые 19-36
            int bettingSum = bettingMap.get(66);
            value = bettingSum * 2;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(62) && result.getNumber() != 0 && result.getNumber() % 2 == 0) { // четные
            int bettingSum = bettingMap.get(62);
            value = bettingSum * 2;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(65) && result.getNumber() % 2 != 0) { // нечетные
            int bettingSum = bettingMap.get(65);
            value = bettingSum * 2;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(51) && 0 < result.getNumber() && result.getNumber() < 13) { // первая дюжина
            int bettingSum = bettingMap.get(51);
            value = bettingSum * 3;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(52) && 12 < result.getNumber() && result.getNumber() < 25) { // вторая дюжина
            int bettingSum = bettingMap.get(52);
            value = bettingSum * 3;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(53) && 24 < result.getNumber() && result.getNumber() < 37) { // третья дюжина
            int bettingSum = bettingMap.get(53);
            value = bettingSum * 3;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(71) && first.contains(result.getNumber())) { // нижняя колонна
            int bettingSum = bettingMap.get(71);
            value = bettingSum * 3;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(72) && second.contains(result.getNumber())) { // средняя колонна
            int bettingSum = bettingMap.get(72);
            value = bettingSum * 3;
            win += value - bettingSum;
            cash += bettingSum;
        }

        if (keySet.contains(73) && third.contains(result.getNumber())) { // верхняя колонна
            int bettingSum = bettingMap.get(73);
            value = bettingSum * 3;
            win += value - bettingSum;
            cash += bettingSum;
        }

        int allCash = moneyManager.getAllCash();
        moneyManager.setAllCash(allCash + cash + win);
        bettingMap.clear();
        return win;
    }


    public Number[] getRouletteNumbersArray() {
        return rouletteNumbersArray;
    }

    public List<Number> getResultList() {
        return resultList;
    }
}
