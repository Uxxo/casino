package application;

import menu.Menu;
import money.MoneyManager;
import poker.PlayPoker;
import roulette.PlayRoulette;
import window.Window;

import java.util.concurrent.CountDownLatch;

public class Application {


    MoneyManager moneyManager = new MoneyManager();
    Window window = new Window();

    Menu menu = new Menu(window.getFrame());
    PlayRoulette playRoulette = new PlayRoulette(window.getFrame(), moneyManager);


    PlayPoker playPoker = new PlayPoker(window.getFrame(), moneyManager);

    boolean flag;
    int valueMenu;

    public void start() {

        while (true) {

            flag = true;
            valueMenu = menu.startMenu();
            switch (valueMenu) {
                case 1:
                    while (flag) {

                        valueMenu = playRoulette.createGamePlay();
                        System.out.println(valueMenu);

                        switch (valueMenu) {
                            case 0:
                                flag = false;
                                window.clearFrame();
                                break;

                            case 1:
                                playRoulette.rotationRoulette();
                                playRoulette.checkBetsMessage();
                                playRoulette.frameRestart();
                        }
                    }

                case 2:
                    while (flag) {

                        valueMenu = playPoker.createGamePlay();
                        switch (valueMenu) {
                            case 0:
                                flag = false;
                                window.clearFrame();
                                break;
                            case 1:
                                playPoker.frameRestart();
                        }
                    }
            }
        }
    }
}
